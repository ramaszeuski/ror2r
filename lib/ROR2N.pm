package ROR2N;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

#    -Debug
use Catalyst qw/
    ConfigLoader
    Static::Simple

    Authentication

    Session
    Session::Store::FastMmap
    Session::State::Cookie

    Log::Dispatch
    I18N
/;

extends 'Catalyst';

our $VERSION = '1.8.0';

# Configure the application.
#
# Note that settings in ror2n.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
    NAME    => 'ROR 2 R',
    VERSION => $VERSION,

    encoding => 'utf8',

    # Disable deprecated behavior needed by old applications
    disable_component_resolution_regex_fallback => 1,
    enable_catalyst_header => 1, # Send X-Catalyst header

    authentication => {
      default_realm => 'users',
      realms        => {
         users => {
            credential => {
               class          => 'Password',
               password_field => 'password',
               password_type  => 'clear'
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::User',
               id_field      => 'login',
               role_column   => 'roles',
            }
         },
      },
   },

   'Plugin::Static::Simple' => {
        include_path => [
            'root/static',
            '/var/lib/ror/records',
            '/var/lib/ror/reports',
        ],
    },

   'Plugin::Session' => {
       expires           => 86400 * 30,
       verify_address    => 1,
       verify_user_agent => 1,
   },

);

# Start the application
__PACKAGE__->setup();


=head1 NAME

ROR2N - Catalyst based application

=head1 SYNOPSIS

    script/ror2n_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<ROR2N::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
