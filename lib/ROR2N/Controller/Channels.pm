package ROR2N::Controller::Channels;
use Moose;
use namespace::autoclean;
use utf8;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Channels - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub json :Local :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash(
        output => 'json',
        json   => {
            data   => [$c->model('DB::Channel')->list( {not_empty => 1} )],
        }
    );
}

sub list :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my %selected = ();

    if ( $args->{relation_id} =~ /\d+/ ) {
        my $relation = $c->model('DB::Relation')->find(
            { id => $args->{relation_id} },
        );
        if ( $relation->channels ) {
            %selected = map { $_ => 1 } (split /\W+/, $relation->channels);
        }
    }

    CHANNEL:
    foreach my $channel ($c->model('DB::Channel')->list( {not_empty => 1} )) {
        $channel->{selected} = $selected{ $channel->{id} };
        push @{ $c->stash->{channels} }, $channel;
    }

    $c->stash->{relation_id}  = $args->{relation_id};
    $c->stash->{general}      = $selected{general} // 0;
    $c->stash->{no_wrapper}   = 1;
}

sub edit :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $channel;

    if ( $args->{id} ) {
        $channel = $c->model('DB::Channel')->find(
            {
                id => $args->{id},
            }
        );

        if ( ! $channel ) {
            $c->response->status(404);
            $c->response->body($c->loc("error.channel.404"));
            return;
        }

        $c->stash->{channel} = $channel;
    }

    if ( $c->req->method eq 'GET') {
        $c->stash(
            no_wrapper => 1
        );
        return;
    }

    my %channel = (
        name => $args->{name},
    );

    if ( $channel ) {
        $channel->update( \%channel );
    }
#    else {
#        $channel = $c->model('DB::Channel')->create( \%channel );
#    }

    $c->stash(
        output => 'json',
        json   => {
            $channel->get_columns(),
        },
    );

}

sub save :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $channel = $c->model('DB::Channel')->find(
        {
            id => ( $args->{pk} || $args->{id} ),
        }
     );

    $channel->update(
        { $args->{name} => $args->{value} },
     );

    $c->stash(
        output => 'json',
        json   => {},
    );
}


=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
