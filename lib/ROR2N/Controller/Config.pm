package ROR2N::Controller::Config;
use Moose;
use namespace::autoclean;
use JSON;
use Net::FTP;

use ROR2N::Tools::PulseAudio;

BEGIN { extends 'Catalyst::Controller::HTML::FormFu'; }

use constant INTERFACE          => 'eth0';
use constant INTERFACES_IN      => '/etc/network/interfaces.d/' . INTERFACE;
use constant INTERFACES_OUT     => '/var/lib/ror/' . INTERFACE;
use constant TEST_FILE_NAME     => '/tmp/ror2r_test.txt';
use constant TEST_FILE_CONTENT  => 'ROR2R FTP Test';
use constant SUPPORTED_HARDWARE => ['zdo', '100v'];

=head1 NAME

ROR2N::Controller::Config - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path  {
    my ( $self, $c ) = @_;

    my $pa = ROR2N::Tools::PulseAudio->new( $c->config->{pulseaudio} );

    $c->stash->{ip}  = $self->read_ipconfig($c);
    $c->stash->{ftp} = $self->read_ftpconfig($c);
    $c->stash->{pa}  = {
        sinks   => [ values %{$pa->{inventory}{sinks}} ],
        sources => [ values %{$pa->{inventory}{sources}} ],
    };

    $c->stash->{supported_hardware} = $c->config->{supported_hardware} || SUPPORTED_HARDWARE();

    # vsechny podporovane hlasice
    $c->stash->{supported_speakers} = $c->model('DB::Speaker')->types();

    # povolene hlasice
    my $allowed_speakers;
    if ( $c->config->{speakers} && ref $c->config->{speakers} eq 'ARRAY' ) {
        $allowed_speakers = $c->config->{speakers};
    }
    else {
        $allowed_speakers = $c->model('DB::Speaker')->types();
    }
    $c->stash->{allowed_speakers} = { map { $_ => 1 } @{$allowed_speakers} };

}

sub save_ip : Local {
    my ($self, $c) = @_;

    my $args = $c->request->params;

    $args->{no_wrapper} = 1;

    if  (my $interfaces = $c->view("HTML")->render($c, 'interfaces.tt2', $args))  {
        open OUT, '>', INTERFACES_OUT;
        print OUT $interfaces;
        close OUT;
    }

    $c->stash(
        output => 'json',
        json   => {},
    );

    $c->model('DB::Config')->find({ id => 'active_user' })->delete();
    $c->logout;
}

sub save_ftp : Local {
    my ($self, $c) = @_;

    my $args = $c->request->params;

    $c->model('DB::Config')->update_or_create(
        {
            id    => 'ftp',
            value => to_json($args),
        }
    );

    if ( ! $args->{test} ) {
        $c->stash(
            output => 'json',
            json   => {},
        );
        return;
    }

    my $ftp = Net::FTP->new($args->{host}, Passive => 1);

    if ( ! $ftp ) {
        $c->response->status(503);
        $c->response->body($c->loc("error.ftp.host"));
        return;
    }

    if ( ! $ftp->login( $args->{login}, $args->{password}) ) {
        $c->response->status(503);
        $c->response->body($c->loc("error.ftp.login"));
        return;
    }

    if ( ! $ftp->cwd($args->{path} ) ) {
        $c->response->status(503);
        $c->response->body($c->loc("error.ftp.path"));
        return;
    }

    # vytvorit testovaci soubor
    open  TEST, '>', TEST_FILE_NAME;
    print TEST TEST_FILE_CONTENT;
    close TEST;

    if ( ! $ftp->put( TEST_FILE_NAME ) ) {
        $c->response->status(503);
        $c->response->body($c->loc("error.ftp.upload"));
        return;
    }

    $ftp->quit();

    $c->stash(
        output => 'json',
        json   => {
            test => 1
        },
    );
    return;

}

sub save_volume : Local {
    my ($self, $c) = @_;

    my $args = $c->request->params;

    my $volume = $self->read_volume( $c );

    $volume->{ $args->{source} } = {
        min => $args->{min},
        max => $args->{max},
    };

    $c->model('DB::Config')->update_or_create(
        {
            id    => 'volume',
            value => to_json( $volume ),
        }
    );

    $c->stash(
        output => 'json',
        json   => {
            rc => 1
        },
    );
    return;

}

sub save_hardware : Local {
    my ($self, $c) = @_;

    my $args = $c->request->params;
    my $pa = ROR2N::Tools::PulseAudio->new( $c->config->{pulseaudio} );

    my $reboot = 0;

    my $speakers = $args->{'speakers[]'};
    $speakers    = [$speakers] if ref $speakers ne 'ARRAY';

    $c->model('DB::Config')->update_or_create(
        {
            id    => 'speakers',
            value => to_json( $speakers ),
        }
    );

    if ( (defined $args->{sink}) && $args->{sink} != $pa->{inventory}{default}{sink} ) {
        $pa->request("set-default-sink " . $args->{sink} );
        $reboot = 1;
    }

    if ( (defined $args->{source}) &&  $args->{source} != $pa->{inventory}{default}{source} ) {
        $pa->request("set-default-source " . $args->{source} );
        $reboot = 1;
    }

    $c->stash(
        output => 'json',
        json   => {
            reboot => $reboot,
        },
    );
    return;

}

sub read_ipconfig {
    my ( $self, $c ) = @_;

    my $config    = {};
    my $interface = INTERFACE;

    if ( -f INTERFACES_IN ) {

        open CONFIG, INTERFACES_IN;
        my $iface;

        LINE:
        while ( <CONFIG> ) {
            next LINE if /^#/;

            chomp;
            s/\s+$//;

            if ( /iface\s+(\S+)\s+\S+\s+(\w+)/ ) {
                ( $iface, $config->{$1}{method} ) = ( $1, $2 );
                next LINE;
            }

            if ( $interface && /(address|netmask|gateway|nameservers)\s+(\S+)/ ) {
                $config->{$iface}{$1} = $2;
            }

        }

        close CONFIG;

        $config = $config->{ $iface };
    }

    if ( ! exists $config->{method} ) {
        $config->{method} = 'dhcp';
    }

    if ( $config->{method} eq 'dhcp' || ! exists $config->{address} ) {
        my $ip = `/sbin/ip -o addr show $interface`;
        if ( $ip =~ /inet\s+(\d+\.\d+\.\d+\.\d+)\/(\d+)\s+brd\s+\d+\.\d+\.\d+\.\d+\s+scope\s+global\s+$interface\\/im ) {
            $config->{address} = $1;

            my $mask  = (2 ** $2 - 1) << (32 - $2);
            $config->{netmask} = join( '.', unpack( "C4", pack( "N", $mask ) ) );
        }
    }

    if ( $config->{method} eq 'dhcp' || ! exists $config->{gateway} ) {
        my $route = `/sbin/ip route list`;
        if ( $route =~ /default via ([\d\.]+) dev $interface/is ) {
            $config->{gateway} = $1;
        }
    }

    if ( ! exists $config->{nameservers} ) {
        my $resolv = `cat /etc/resolv.conf`;
        if ( $resolv =~ /nameserver\s+([\d\.]+)/is ) {
            $config->{nameservers} = $1;
        }
    }


    return $config;

}

sub read_ftpconfig {
    my ( $self, $c ) = @_;

    my $cfg = $c->model('DB::Config')->find(
        {
            id => 'ftp',
        }
    );

    return $cfg ?  from_json( $cfg->value() ) : {}

}

sub read_volume {
    my ( $self, $c ) = @_;

    my $cfg = $c->model('DB::Config')->find(
        {
            id => 'volume',
        }
    );

    $cfg = $cfg ?  from_json( $cfg->value() ) : {};

    SRC:
    foreach my $src ( qw(out mic) ) {
        if ( ! exists $cfg->{ $src } ) {
            $cfg->{ $src } = {
                min => 0,
                max => 100,
            };
        }
    }

    return $cfg;

}

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
__END__
