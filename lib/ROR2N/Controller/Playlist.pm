package ROR2N::Controller::Playlist;
use Moose;
use namespace::autoclean;

use Date::Manip;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Playlist - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub list :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $conditions = {
        relation_id => $args->{relation_id},
    };

    my @playlist = ();

    my $items = $c->model('DB::Playlist_view')->search(
        $conditions,
        {
            order_by => ['position']
        }
    );

    my $position = 0; #je to lepsi pro pripad smazani

    RECORD:
    while ( my $item = $items->next ) {

        push @playlist, {
            $item->get_columns,
            position => $position++,
            duration_hms => $item->record_duration_hms,
        };
    }

    $c->stash(
        no_wrapper  => 1,
        relation_id => $args->{relation_id},
        records     => \@playlist,
    );
}


sub add :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    $c->stash->{output} = 'json';

    # relace
    my $relation = $c->model('DB::Relation')->find(
        { id => $args->{relation_id}},
    );

    if ( ! $relation ) {
        $c->response->status(404);
        $c->response->body($c->loc("error.relation.404"));
        return;
    }

    # zaznam
    my $record = $c->model('DB::Record')->find(
        { id => $args->{record_id}},
    );

    if ( ! $record ) {
        $c->response->status(404);
        $c->response->body($c->loc("error.record.404"));
        return;
    }

    my $duration = $relation->duration + $record->duration;

    # kolize s jinou relaci

    if ( $relation->scheduled ) {
        my $collision = $c->model('DB::Relation')->collision(
            {
                relation => $relation,
                begin    => $relation->scheduled,
                duration => $duration,
            }
        );

        if ( $collision) {
            $c->response->status(409);
            $c->response->body($c->loc("error.relation.409"));
            return;
        }
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    # posledni zaznam #TODO: relation->playlist
    my $last = $c->model('DB::Playlist')->search(
        { relation_id => $args->{relation_id} },
        {
            order_by => [ {'-desc' => 'position'} ],
            columns  => [ 'position' ],
        }
    )->first;

    $c->model('DB::Playlist')->create(
        {
            relation_id => $args->{relation_id},
            record_id   => $args->{record_id},
            position    => ( $last ? $last->position + 1 : 1 ),
        }
    );

    $relation->update( { duration => $duration } );

    $scope_guard->commit;

    # jeste jdnou kvuli konci
    if ( $relation->id ) {

        $relation = $c->model('DB::Relation_view')->find(
            { id => $relation->id },
        );

        # aktivace / deaktivace
        $c->model('At')->delete( { id => $relation->id } );
        if ( $relation->configured ) {
            $c->model('At')->add(
                {
                    id   => $relation->id,
                    time => $relation->scheduled,
                }
            );
        }

        $c->stash->{json} = {
            status => $relation->status,
            end    => UnixDate( $relation->scheduled_end, '%H:%M')
        };
    }
    else { # playlist ziveho vysilani
        $c->stash->{json} = {};
    }

}

sub reorder :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $position = 0;

    ITEM:
    foreach my $id ( @{ $args->{'playlist[]'}} ) {
        $id =~ s/\D+//g;
        my $item = $c->model('DB::Playlist')->find({ id => $id }) || next;
        $item->update( { position => $position++ } );
    }

    $c->stash(
        output => 'json',
        json   => {},
    );

}

sub delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $storage = ROR2N::Storage->new( $c->config->{storage}{path});

    $c->stash->{output} = 'json';

    my $item = $c->model('DB::Playlist')->search(
        {
            id => $args->{id},
        }
    )->first();

    # relace
    my $relation = $c->model('DB::Relation')->find(
        { id => $item->relation_id},
    );

    if ( ! $relation ) {
        $c->stash->{json}{error} = 'relation 404';
        return;
    }

    # zaznam
    my $record = $c->model('DB::Record')->find(
        { id => $item->record_id },
    );

    if ( ! $record ) {
        $c->stash->{json}{error} = 'record 404';
        return;
    }

    my $duration = $relation->duration - $record->duration;

    my $items = $c->model('DB::Playlist')->search(
        {
            relation_id => $relation->id,
            id          => { '!=' => $item->id },
        },

        {
            order_by => ['position']
        }
    );

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

        $item->delete;

        my $position = 0;

        ITEM:
        while ( $item = $items->next() ) {
            $item->update( { position => $position++ });
        }

        $relation->update( { duration => $duration } );

        if ( $record->category_id eq 'relations' ) {
            $record->update(
                { deleted => \'current_timestamp' },
            );
            $storage->delete( $args->{id}, 0 );
        }

    $scope_guard->commit;

    # jeste jdnou kvuli konci
    if ( $relation->id ) {
        $relation = $c->model('DB::Relation_view')->find(
            { id => $relation->id },
        );

        # aktivace / deaktivace
        $c->model('At')->delete( { id => $relation->id } );
        if ( $relation->configured ) {
            $c->model('At')->add(
                {
                    id   => $relation->id,
                    time => $relation->scheduled,
                }
            );
        }

        $c->stash->{json} = {
            status => $relation->status,
            end    => UnixDate( $relation->scheduled_end, '%H:%M')
        };
    }
    else { # playlist ziveho vysilani
        $c->stash->{json} = {};
    }

}

=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
