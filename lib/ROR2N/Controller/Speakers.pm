package ROR2N::Controller::Speakers;
use Moose;
use namespace::autoclean;

use Date::Manip;
use POSIX qw(strftime);
use JSON;

BEGIN { extends 'Catalyst::Controller'; }

use constant MAX_LOG_ROWS    => 50;
use constant UNDEFINED_VALUE => '-';

=head1 NAME

ROR2N::Controller::Speakers - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
}

sub json :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    $c->stash( output => 'json' );

    my $conditions = {};

    if ( $args->{locality_id} ) {
        $conditions->{locality_id} = $args->{locality_id};
    }

    if ( $args->{channel_id} ) {
        $conditions->{channel_id} = $args->{channel_id};
    }

    my @speakers;

    my $speakers = $c->model('DB::Speaker')->search(
        $conditions,
        {
            order_by => ['me.id'],
            prefetch => ['locality', 'channel'],
        },
    );

    my $bounds = {
        east  => 0,
        west  => 0,
        north => 0,
        south => 0,
    };

    SPEAKER:
    while ( my $speaker = $speakers->next() ) {

        my @class = (
            'type_' . $speaker->type,
            'status_' . $speaker->status,
        );
        push @class, 'active' if $speaker->active;

        my $description = ( $speaker->name || $c->loc('Speaker') )
                        . ' / ' . $c->loc('speaker.type.' . $speaker->type);

        my %speaker = (
            $speaker->get_columns,
            lat           => $speaker->lat,
            lng           => $speaker->lng,
            status        => $speaker->status,
            locality_name => $speaker->locality_id ?  $speaker->locality->name : '',
            channel_name  => $speaker->channel->name,
            type_name     => $c->loc('speaker.type.' . $speaker->type),
            description   => $description,
            DT_RowClass   => (join ' ', @class),
        );

        push @speakers, \%speaker;

        if ( $speaker{lng} && $speaker{lat} ) {
            $bounds->{east}  ||= $speaker{lng};
            $bounds->{west}  ||= $speaker{lng};
            $bounds->{north} ||= $speaker{lat};
            $bounds->{south} ||= $speaker{lat};
            $bounds->{east}  =   $speaker{lng} if $speaker{lng} > $bounds->{east};
            $bounds->{west}  =   $speaker{lng} if $speaker{lng} < $bounds->{west};
            $bounds->{north} =   $speaker{lat} if $speaker{lat} > $bounds->{north};
            $bounds->{south} =   $speaker{lat} if $speaker{lat} < $bounds->{south};
        }

    }

    $c->stash(
        output => 'json',
        json   => {
            data   => \@speakers,
            bounds => $bounds,
        }
    );
}

sub edit :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    my $speaker;

    if ( $args->{id} ) {
        $speaker = $self->get( $c, $args->{id}) // return;
    }

    if ( $c->req->method eq 'GET') {
        my @ids      = $c->model('DB::Speaker')->free_ids();

        if ( $speaker ) {
            push @ids, $speaker->id;
        }

        my $types;
        if ( $c->config->{speakers} && ref $c->config->{speakers} eq 'ARRAY' ) {
            $types = $c->config->{speakers};
        }
        else {
            $types = $c->model('DB::Speaker')->types();
        }

        $c->stash(
            ids        => [ sort { $a <=> $b } @ids ],
            channels   => [ $c->model('DB::Channel')->list() ],
            types      => [
                map { {id => $_, name => $c->loc("speaker.type.$_")} } @{ $types }
            ],
            localities => [ $c->model('DB::Locality')->list() ],
            no_wrapper => 1
        );
        return;
    }

    my %speaker = (
        id   => $args->{new_id},
        name => $args->{name},
        type => $args->{type},
        status      => ( $args->{type} == 1 || $args->{type} == 4) ? 1 : 0, #jednosmer vzdy OK TODO: konfig?
        description => $args->{description},
        locality_id => $args->{locality_id},
        channel_id  => $args->{channel_id},
    );

    if ( $speaker ) {
        $speaker->update( \%speaker );
    }
    else {
        $speaker{gps} = $args->{gps};

        $speaker = $c->model('DB::Speaker')->create( \%speaker );
    }

    $c->stash(
        output => 'json',
        json   => {
            $speaker->get_columns(),
            lat => $speaker->lat,
            lng => $speaker->lng,
        },
    );

}

sub volume :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args    = $c->request->params();
    my $speaker = $self->get( $c, $args->{id} );

    my @channels;
    my %last_data;

    my $last_log = $speaker->logs(
        {
            speaker_type => $speaker->type,
            data => { like => '%volume%' },
        },
        {
            order_by => [ {'-desc' => 'time'} ],
        }
    )->first;

    if ( $last_log ) {
        %last_data = %{ $last_log->data };
    }

    INPUT:
    foreach my $input ( @{ $speaker->inputs() } ) {
        next INPUT if ! $input->{type};
        next INPUT if $input->{type} ne 'volume';
        my $id = $input->{id};
        $id =~ s/\D+//;
        push @channels, {
            id   => $id,
            name => $c->loc(join '.',
                'speaker.input',
                $speaker->type,
                $input->{id},
                'desc'
            ),
            value => $speaker->last_data
                        ? $speaker->last_data->{ $input->{id}}
                        : $last_data{$input->{id}}
        }
    }

    $c->stash(
        no_wrapper => 1,
        channels   => \@channels,
    );
}

sub save :Local :Args(0) { #TODO: nejspis mrtvola
    my ( $self, $c ) = @_;

    my $args    = $c->request->params();
    my $speaker = $self->get( $c, $args->{pk}) // return;

    $speaker->update(
        { $args->{name} => $args->{value} },
     );

    $c->stash(
        output => 'json',
        json   => {
            $speaker->get_columns(),
            lat => $speaker->lat,
            lng => $speaker->lng,
        },
    );
}

sub delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args    = $c->request->params();
    my $speaker = $self->get( $c, $args->{id}) // return;

    $speaker->delete();

    $c->stash(
        output => 'json',
        json   => {},
    );
}

sub test :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args    = $c->request->params();
    my $speaker = $self->get( $c, $args->{id}) // return;

    #MQTT s timeoutem

    $c->stash(
       $speaker->get_columns(),
        no_wrapper => 1
    );
}

sub status :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args    = $c->request->params();
    my $speaker = $self->get( $c, $args->{id}) // return;

    my @inputs = (
#        { id => 'datetime', name => $c->loc('Date and time') },
    );

    INPUT:
    foreach my $input ( @{ $speaker->inputs() } ) {
        if ( $speaker->input_names && $speaker->input_names->{ $input->{id} } ) {
            $input->{name} = $speaker->input_names->{ $input->{id} };
        }
        else {
            my $i18n_key = join '.',
                'speaker.input', $speaker->type, $input->{id};
            my $i18n     = $c->loc( $i18n_key );

            if ( $i18n ne $i18n_key ) {
                $input->{name} = $i18n;
            }
            else {
                $input->{name} = $input->{id};
            }


        }

        push @inputs, $input;
    }

    $c->stash(
        inputs  => \@inputs,
        speaker => {
            $speaker->get_columns(),
            type_name  => $c->loc('speaker.type.' . $speaker->type),
        },
        no_wrapper => 1
    );

}

sub log :Local {
    my ( $self, $c ) = @_;

    my $args    = $c->request->params();
    my $speaker = $self->get( $c, $args->{speaker_id}) // return;

    my @logs = ();

    my $logs = $speaker->logs(
        {
            speaker_type => $speaker->type,
        },
        {
            order_by => [{ -desc => 'time'}],
            rows     => MAX_LOG_ROWS,
        }
    );

    LOG:
    while ( my $log = $logs->next() ) {
        my %log = (
            datetime   => UnixDate( $log->time, '%d.%m.%Y %H:%M'),
            event_name => $log->data->{event}
                        ? $c->loc('speaker.event.' . $log->data->{event}) : ''
        );

        INPUT:
        foreach my $input ( @{ $speaker->inputs() } ) {
            my $value  = $log->data->{ $input->{id} };

            if ( ! defined $value ) {
                $log{ $input->{id} } = UNDEFINED_VALUE;
                next INPUT;
            }

            my $alert = 0;

            if ( ref $input->{alert} ) {
                $alert = $input->{ alert }( $value );
            }

            if ( ref $input->{formatter} ) {
                $value = $input->{ formatter }( $value );
            }

            # preklady nekterych hodnot
            my $i18n_key = join '.',
                'speaker.input', $speaker->type, $input->{id}, $value;
            my $i18n     = $c->loc( $i18n_key );
            $value = $i18n if $i18n ne $i18n_key;

            # v pripade problemu je to struktura
            if ( $alert ) {
                $log{ $input->{id} } = {
                    value => (  ) ? $value : $i18n,
                    alert => $alert,
                }
            }
            else {
                $log{ $input->{id} } = $value;
            }

        }

        push @logs, \%log;
    }

    $c->stash(
        output => 'json',
        json   => {
            data => \@logs,
        }
    );
}

sub get {
    my ( $self, $c, $id ) = @_;

    my $speaker = $c->model('DB::Speaker')->find(
        {
            id => $id,
        }
    );

    if ( ! $speaker ) {
        $c->response->status(404);
        $c->response->body($c->loc("error.speaker.404"));
        return;
    }

    $c->stash->{speaker} = $speaker;
    return $speaker;
}


=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
