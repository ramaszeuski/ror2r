package ROR2N::Controller::Import;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Import - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched ROR2N::Controller::Import in Import.');
}

sub microfone :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    $c->stash(
        relation_id => $args->{relation_id},
        no_wrapper  => 1,
    );
}

sub upload :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    if ( $c->req->method eq 'GET' ) {
        $c->stash( no_wrapper => 1 );
        return;
    }

    my $upload = $c->request->upload('0');
    if ( ! $upload ) {
        $c->response->status('412');
        return;
    }

    my $format;
    if ( $upload->type =~ m{audio/(mp3|mp4|mpeg|wav|x-wav|x-ms-wma|x-m4a)}smx) {
        $format = $1;
    }
    else {
        $c->response->status('415');
        return;
    }

    my $storage = ROR2N::Storage->new( $c->config->{storage}{path} );
    my $mp3     = $storage->store( $upload->tempname );
    my $record  = $c->model('DB::Record')->find_or_create(
        {
            id          => $mp3->{id},
            category_id => $c->config->{storage}{default_category},
            duration    => $mp3->{duration},
            name        => $mp3->{properties}{title} || $upload->filename,
            properties  => $mp3->{properties},
        }
    );
    # obnoveni smazanych opakovanym nahranim
    $record->update({ deleted => undef });

    $c->stash(
        output => 'json',
        json   => { $record->get_columns },
    );
}


=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;


1;
