package ROR2N::Controller::Alerts;
use Moose;
use namespace::autoclean;
use Date::Manip;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Alerts - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub clear :Local :Args(0) {
    my ( $self, $c ) = @_;

    $c->model('DB::Relation')->alerts_clear();

    $c->stash(
        output => 'json',
        json   => {
            rc => 0,
        }
    );
}

sub list :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my @alerts = ();

    my $relations = $c->model('DB::Relation')->alerts_list();

    ALERT:
    while ( my $relation = $relations->next() ) {

        my $description = $c->loc(
            join '_', ('alert_relation', $relation->report->{status_id})
        );

        push @alerts, {
            status_id   => $relation->report->{status_id},
            timestamp   => UnixDate($relation->scheduled, '%d.%m.%Y %H:%M:%S'),
            description => sprintf($description, $c->loc($relation->name)),
            url         => '/relations/?report=' . $relation->id
        };
    }

    $c->stash(
        alerts     => \@alerts,
        no_wrapper => 1,
    );
}

1;
