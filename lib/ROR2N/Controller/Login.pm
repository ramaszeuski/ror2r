package ROR2N::Controller::Login;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

use constant HOME_PAGE => '/';

=head1 NAME

ROR2N::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $user = $c->model('DB::User')->search(
        {
            deleted => undef,
            login   => $args->{login},
        },
    )->first();

    my @response;

    if ( ! $user ) {
        push @response, ['login', 0, 'ajaxUserNotExists'];
    }
    elsif ( ! $user->active ) {
        push @response, ['login', 0, 'ajaxUserDisabled'];
    }
    elsif ( $user->password ne $args->{password} ) {
        push @response, ['password', 0, 'ajaxBadPassword'];
    }
    else {
        $c->authenticate(
            {
                login    => $user->login,
                password => $user->password,
            }
        );
    }

    $c->stash(
        output => 'json',
        json   => \@response,
    );

#        $c->model('DB::Log')->create(
#            {
#                event         => 'user_login',
#                user_id       => $c->user->id,
#                address       => $c->req->address,
#            }
#        );


}

sub logout
:Global
:Args(0)
{
    my ( $self, $c ) = @_;
#    $c->model('DB::Log')->create(
#        {
#            event         => 'user_logout',
#            user_id       => $c->user->id,
#            address       => $c->req->address,
#        }
#    );

    $c->model('DB::Config')->search( { id => 'active_user'})->delete();
    $c->delete_session('session expired');
    $c->logout;
    $c->response->redirect(HOME_PAGE);
}

# METODY UMISTENE TADY ABY NEVYZADOVALI AUTENTIFIKACE

sub reboot
:Global
:Args(0)
{
    my ( $self, $c ) = @_;
    $c->stash->{reboot}   = 1;
    $c->stash->{template} = 'reboot.tt2';
}

sub status :Global :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash(
        output => 'json',
        json   => {
            status => 'OK',
        }
    );
}

sub validator : Path {
    my ( $self, $c ) = @_;
}
=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
__END__

