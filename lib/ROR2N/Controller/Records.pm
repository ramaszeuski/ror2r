package ROR2N::Controller::Records;
use Moose;
use namespace::autoclean;
use Audio::MPD;
use ROR2N::Storage;
use utf8;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Records - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
}

sub json :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $conditions = {
        deleted     => undef,
        category_id => $args->{category_id},
    };

    my @records;

    my $records = $c->model('DB::Record')->search(
        $conditions,
        {
            order_by => ['name']
        }
    );

    RECORD:
    while ( my $record = $records->next() ) {

        my @class = ();
        push @class, 'active' if $record->active;

        # uvodni a ukoncovaci hlasky jen pro ROR/ZDO
        if ( $c->config->{hardware}{type} eq 'zdo') {
            push @class, 'intro_begining' if $record->properties->{intro_begining};
            push @class, 'intro_ending' if $record->properties->{intro_ending};
        }

        my $created = $record->created;
        $created =~ s/:\d\d$//;

        push @records, {
            $record->get_columns,
            created      => $created,
            duration_hms => $record->duration_hms,
            DT_RowClass  => (join ' ', @class),
        };
    }

    $c->stash(
        output => 'json',
        json   => {
            data => \@records,
        }
    );
}

sub list :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $conditions = {
        active      => 1,
        deleted     => undef,
        category_id => $args->{category_id},
    };

    my @records;

    my $records = $c->model('DB::Record')->search(
        $conditions,
        {
            order_by => ['name']
        }
    );

    RECORD:
    while ( my $record = $records->next() ) {
        push @records, {
            $record->get_columns,
            duration_hms => $record->duration_hms,
        };
    }

    $c->stash(
        no_wrapper => 1,
        records    => \@records,
    );
}

sub get :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $record = $c->model('DB::Record')->find(
        {
            id => $args->{id},
        }
    );

    if ( ! $record ) {
        $c->response->status(404);
        $c->response->body($c->loc("error.record.404"));
        return;
    }

    $c->stash(
        output => 'json',
        json   => { $record->get_columns() },
    );
}

sub save :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $record = $c->model('DB::Record')->find(
        {
            id => $args->{pk},
        }
    );

    $record->update(
        { $args->{name} => $args->{value} },
     );

    $c->stash(
        output => 'json',
        json   => {},
    );
}

sub delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $storage = ROR2N::Storage->new( $c->config->{storage}{path});

    my $record = $c->model('DB::Record')->find(
        {
            id => $args->{id},
        }
    );

    $record->update(
        { deleted => \'current_timestamp' },
     );

    $record->playlist_items->delete();

    $storage->delete( $args->{id}, $args->{hard} );

    $c->stash(
        output => 'json',
        json   => {},
    );
}

sub intro :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $record = $c->model('DB::Record')->find(
        {
            id => $args->{id},
        }
    );

    my $properties = $record->properties;

    if ( $args->{type} eq 'begining' ) {
        delete $properties->{intro_ending};
        $properties->{intro_begining} = 1;
    }
    elsif  ( $args->{type} eq 'ending' ) {
        delete $properties->{intro_begining};
        $properties->{intro_ending} = 1;
    }
    else {
        delete $properties->{intro_begining};
        delete $properties->{intro_ending};
    }

    $record->update(
        { properties => $properties },
     );
#    my $record = $c->model('DB::Config')->update_or_create(
#        {
#            id    => 'intro',
#            value => $args->{id},
#        }
#    );

    $c->stash(
        output => 'json',
        json   => {},
    );
}

sub set_default_stream :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    # zrusit stavajici default stream
    my $default = $c->model('DB::Record')->search(
        {
            category_id => ['stream'],
            properties  => { like => '%"default":1%'},
        },
    );
    RECORD:
    while ( my $stream = $default->next ) {
        my $properties = $stream->properties;
        $properties->{default} = 0;
        $stream->update( { properties => $properties } );
    }

    my $stream = $c->model('DB::Record')->find(
        { id => $args->{id} },
    );
    my $properties = $stream->properties;
    $properties->{default} = 1;
    $stream->update( { properties => $properties } );

    $c->stash(
        output => 'json',
        json   => {},
    );
}

=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
