package ROR2N::Controller::Localities;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Localities - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    $c->response->body('Matched ROR2N::Controller::Localities in Localities.');
}

sub json :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $conditions = {};

    my @localities;

    my $localities = $c->model('DB::Locality')->search(
        $conditions,
        {
            order_by => ['name']
        }
    );

    LOCALITY:
    while ( my $locality = $localities->next() ) {

        my @class = ();
        push @class, 'active' if $locality->active;

        my %locality = (
            $locality->get_columns,
            DT_RowClass  => (join ' ', @class),
        );

        push @localities, \%locality;

    }

    $c->stash(
        output => 'json',
        json   => {
            data   => \@localities,
        }
    );
}

sub edit :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $locality;

    if ( $args->{id} ) {
        $locality = $c->model('DB::Locality')->find(
            {
                id => $args->{id},
            }
        );

        if ( ! $locality ) {
            $c->response->status(404);
            $c->response->body($c->loc("error.locality.404"));
            return;
        }

        $c->stash->{locality} = $locality;
    }

    if ( $c->req->method eq 'GET') {
        $c->stash(
            no_wrapper => 1
        );
        return;
    }

    my %locality = (
        name => $args->{name},
    );

    if ( $locality ) {
        $locality->update( \%locality );
    }
    else {
        $locality = $c->model('DB::Locality')->create( \%locality );
    }

    $c->stash(
        output => 'json',
        json   => {
            $locality->get_columns(),
        },
    );

}

sub delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $locality = $c->model('DB::Locality')->find(
        {
            id => $args->{id},
        }
    );

    if ( $locality->speakers->count() ) {
        $c->response->status(409);
        return;
    }

    $locality->delete();

    $c->stash(
        output => 'json',
        json   => {},
    );
}
=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
