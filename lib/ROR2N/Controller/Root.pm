package ROR2N::Controller::Root;
use Moose;
use namespace::autoclean;
use utf8;

use JSON;
use POSIX qw(strftime);

use constant INACTIVE_TIMEOUT        => 300;
use constant UPDATE_ACTIVE_USER_TIME => 60;

BEGIN { extends 'Catalyst::Controller' }

#
# Sets the actions in this controller to be registered with no prefix
# so they function identically to actions created in MyApp.pm
#
__PACKAGE__->config(namespace => '');

=head1 NAME

ROR2N::Controller::Root - Root Controller for ROR2N

=head1 DESCRIPTION

[enter your description here]

=head1 METHODS

=head2 index

The root page (/)

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
    my $args = $c->request->params();

    if ( $c->config->{hardware}{type} ne 'zdo') {
        $c->response->redirect('/records/');
    }

    # relace
    my $relation = $c->model('DB::Relation')->find(
        { id => 0 },
    );

    # potrebujeme vedet ktery zaznam je intro kvuli zvyrazneni ####TODO:: PREDELAT LOGIKU INTER!!!
    my $ending = $c->model('DB::Config')->search(
        { id => 'intro' }
    )->first();

    $ending = $ending ? $ending->value : '';

    # vzdy nacteme intra a streamy
    my $records = $c->model('DB::Record')->search(
        {
            active      => 1,
            category_id => ['intro', 'stream'],
            deleted     => undef,
        },
        {
            order_by => [{-desc =>'priority'}, 'name'],
            columns  => ['id', 'name', 'category_id', 'properties' ]
        }
    );

    RECORD:
    while ( my $record = $records->next ) {

        my $properties = $record->properties;

        if ( $record->category_id eq 'intro' ) {
            next RECORD if !
                ( $properties->{intro_begining} || $properties->{intro_ending} );

            push @{ $c->stash->{intros} }, {
                $record->get_columns,
                properties => $properties,
                selected   => $relation->has_intro( $record->id ),
            };
        }

        if ( $record->category_id eq 'stream' ) {
            push @{ $c->stash->{streams} }, {
                id   => $record->id,
                name => $record->name,
            };

            if ( $properties->{default} ) {
                $c->stash->{default_stream_configured} = 1;
            }

        }
    }

}

sub auto :Private {
    my ($self, $c) = @_;

    # nacteni casti configurace z databazi
    DB_CONFIG:
    foreach my $db_config ( $c->model('DB::Config')->all() ) {
        $c->config->{ $db_config->id } = from_json( $db_config->value );
    }

    my $active_user;

    if ( $c->controller eq $c->controller('Login') ) {
        return 1;
    }

    return if ! $c->user_exists;

    my %me = (
        sid   => $c->sessionid,
        login => $c->user->login,
        ip    => $c->req->address,
        ua    => $c->req->user_agent,
        time  => time,
    );

    $active_user = $c->model('DB::Config')->find( { id    => 'active_user' });

    if ( $active_user ) {
        $c->stash->{active_user} = from_json( $active_user->value );
        my $timeout = $me{time} - $c->stash->{active_user}{time};

        if ( $c->stash->{active_user}{sid} ne $c->sessionid) {

            if ( $timeout < INACTIVE_TIMEOUT ) {
                $c->stash->{occupied_by_another_user} = 1;
            }
            else {
                $active_user->update( { value => to_json( \%me )} );
            }

        }
        elsif ( $timeout > UPDATE_ACTIVE_USER_TIME ) {
            $active_user->update( { value => to_json( \%me )} );
        }


    }
    else {
        $active_user = $c->model('DB::Config')->find_or_create({
            id    => 'active_user',
            value => to_json( \%me ),
        });
    }

    ROLE:
    foreach my $role ( $c->user->roles ) {
        $c->stash->{role}{$role} = 1;
    }

    $c->stash->{time} = strftime "%H:%M", localtime;

    # pocet alertu
    $c->stash->{alerts} = $c->model('DB::Relation')->alerts_count();


    return 1;
}

sub default :Path {
    my ( $self, $c ) = @_;

# Jednoduchy dhandler
    my $template = $c->request->path;
    $template  =~ s{/$}{\.html};

    my $file = join '/', (
        $c->config->{home} // '',
        $c->config->{static}{include_path}[0],
        $template,
    );

    if ( -f $file ) {
        $c->stash->{template} = $template;
    }
    else {
        $c->response->body( 'Page not found ' . $file );
        $c->response->status(404);
    }
}

=head2 end

Attempt to render a view, if needed.

=cut

sub end : Private {
    my ($self, $c) = @_;

    if ( scalar @{ $c->error } ) {
        $c->stash->{errors}   = $c->error;
        for my $error ( @{ $c->error } ) {
            $c->log->error($error);
        }
        $c->clear_errors;
    }

    if ( $c->request->param('no_wrapper') ) {
        $c->stash->{no_wrapper} = 1;
    }

    if ( $c->res->status =~ /^[45]/ ) {
        $c->response->content_type('text/html; charset=utf-8');
    }
    elsif (
            ( $c->req->param('output') && $c->req->param('output') eq 'json' )
         || ( $c->stash->{output} && $c->stash->{output} eq 'json' )
    ) {
        $c->forward('View::JSON');
    }
    elsif ( (! $c->res->body) && ($c->res->status !~ /^3/) ) {
        $c->forward('View::HTML');
    }
}

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__

