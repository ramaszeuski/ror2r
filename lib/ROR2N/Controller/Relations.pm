package ROR2N::Controller::Relations;
use Moose;
use namespace::autoclean;

use Date::Manip;


BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Relations - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
    my $args = $c->request->params();

    if ( $args->{report} ) {
        $c->stash->{load_report} = $args->{report};
    }

}

sub json :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    $c->stash( output => 'json' );

    my $conditions = {
        active      => 1,
        deleted     => undef,
        report      => undef,
    };

    if ( $args->{archive} ) {
        $conditions->{report} = { '!=' => undef };
    }


    if ( ! $c->stash->{role}{tests} ) {
        $conditions->{properties} = { 'not like' => '%"test":"1"%'};
    }

    my @relations = ();

    my $relations = $c->model('DB::Relation_view')->search(
        $conditions,
        {
            order_by => [{-desc => 'scheduled'}]
        }
    );

    RECORD:
    while ( my $relation = $relations->next() ) {

        push @relations, {
            $relation->get_columns,
            name        => $c->loc($relation->name),
            DT_RowClass => 'report_status_' . $relation->status,
            timespan    => $c->loc( $relation->timespan() ),
            status      => $relation->status,
            test        => $relation->testing,
        };
    }

    $c->stash(
        output => 'json',
        json   => {
            data   => \@relations,
        }
    );
}

sub relation :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    $c->stash->{no_wrapper} = 1;

    if ( $args->{id} ) {
        my $relation = $c->model('DB::Relation_view')->find(
            { id => $args->{id} },
        );
        $c->stash->{relation} = {
            $relation->get_columns,
            properties => $relation->properties,
            status     => $relation->status,
            obsolete   => $relation->obsolete,
            time       => $relation->scheduled_time,
            day        => $relation->scheduled_day,
            end        => $relation->scheduled_end_time,
        };
    }
}

sub add :Local :Args(0) {
    my ( $self, $c ) = @_;
    $c->stash(
        no_wrapper => 1,
        template   => 'relations/edit.tt2',
    );
}

sub clone :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    $self->get( $c, $args->{id} );

    $c->stash(
        no_wrapper => 1,
        clone      => 1,
        template   => 'relations/edit.tt2',
    );
}

sub edit :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    $self->get( $c, $args->{id} );

    $c->stash->{no_wrapper} = 1;
}

sub save :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();
    my $relation;
    my $properties;
    my $new = 0;

    if ( $args->{id} ) {
        $relation = $self->get( $c, $args->{id}) // return;
        $properties = $relation->properties;
    }

    $properties->{test} = $args->{test} // 0;

    my %relation = (
        name       => $args->{name},
        properties => $properties,
    );

    if ( $c->config->{hardware}{type} ne 'zdo' ) {
        $relation{channels} = 'general';
    }

    if ( $args->{clone} ) {
        # dulezite pri klonovani archovovaneho ziveho vysilani
        delete $relation{properties}->{live};
        # neklonovat nemsazane alerty
        delete $relation{properties}->{alert};
        $relation{channels} = $relation->channels;
        $relation{duration} = $relation->duration;

        PLAYLIST:
        foreach my $pi ( $relation->playlist_items() ) {
            push @{ $relation{playlist_items}}, {
                record_id => $pi->record_id,
                position  => $pi->position,
            };
        }

        $relation = undef;
    }

    if ( $relation ) {
        $relation->update( \%relation );
    }
    else {
        $new = 1;
        $relation = $c->model('DB::Relation')->create( \%relation );
    }

    $c->stash(
        output => 'json',
        json   => {
            $relation->get_columns(),
            status  => $relation->status,
            new     => $new,
        },
    );

}

sub schedule :Local :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{output} = 'json';

    my $args      = $c->request->params();
    my $relation  = $self->get($c, $args->{id});
    my $scheduled = UnixDate(ParseDate('epoch ' . $args->{scheduled} / 1000), '%Y-%m-%d %H:%M:00');

    #TODO: nevalidni

    my $collision = $c->model('DB::Relation')->collision(
        {
            relation => $relation,
            begin    => $scheduled,
        }
    );

    if ( $collision ) {
        $c->response->body( $c->loc("error.relation.409") . ' - '.$collision->name );
#       $c->response->body( $collision->name );
        $c->response->status(409);
        return;
    }

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $relation->update({ scheduled => $scheduled, });

    # aktivace / deaktivace
    $c->model('At')->delete( { id => $relation->id } );

    if ( $relation->configured ) {
        $c->model('At')->add(
            {
                id   => $relation->id,
                time => $relation->scheduled,
            }
        );
    }

    $scope_guard->commit;

    $c->stash->{json} = { id => $relation->id };
}

sub schedule_cancel :Local :Args(0) {
    my ( $self, $c ) = @_;

    $c->stash->{output} = 'json';

    my $args        = $c->request->params();
    my $relation    = $self->get($c, $args->{id});
    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $relation->update({ scheduled => undef });

    # aktivace / deaktivace
    $c->model('At')->delete( { id => $relation->id } );

    $scope_guard->commit;

    $c->stash->{json} = { id => $relation->id };
}

sub channel :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args     = $c->request->params();
    my $relation = $self->get($c, $args->{id});

    if ( $args->{add} ) {
        $relation->add_channel( $args->{channel_id} );
    }
    else {
        $relation->del_channel( $args->{channel_id} );
    }

    $c->model('At')->delete( { id => $relation->id } );
    if ( $relation->configured ) {
        $c->model('At')->add(
            {
                id   => $relation->id,
                time => $relation->scheduled,
            }
        );
    }

    $c->stash(
        output => 'json',
        json   => { status => $relation->status },
    );
}

sub delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args     = $c->request->params();
    my $relation = $self->get($c, $args->{id});

    $c->model('At')->delete( { id => $relation->id } );

    $relation->update(
        { deleted => \'current_timestamp' },
     );

    $c->stash(
        output => 'json',
        json   => {},
    );
}

sub save_intros :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    # relace
    my $relation = $self->get($c, $args->{relation_id});

    my $properties = $relation->properties;
    my $records = $args->{'records[]'};
    $records = [ $records ] if ref $records ne 'ARRAY';

    $properties->{intros}{$args->{type}} = $records ;

    $relation->update({ properties => $properties });

    $c->stash(
        output => 'json',
        json   => {},
    );
}

sub get {
    my ( $self, $c, $id ) = @_;

    my $relation = $c->model('DB::Relation')->find(
        {
            id => $id,
        }
    );

    if ( ! $relation ) {
        $c->response->status(404);
        $c->response->body($c->loc("error.relation.404"));
        return;
    }

    $c->stash->{relation} = $relation;

    return $relation;
}

sub report :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    $c->stash->{no_wrapper} = 1;

    if ( ! $args->{id} ) {
        $c->stash->{report} = {};
        return;
    }

    my $relation = $c->model('DB::Relation')->find(
        { id => $args->{id} },
    );

    my $report = $relation->report();

    my %speakers = $report->{speakers} ? %{ $report->{speakers} } : ();
    my @speakers = ();

    SPEAKER:
    foreach my $id ( sort keys %speakers ) {
        push @speakers, {
            id => $id,
            %{ $speakers{$id} }
        };
    }

    $c->stash->{report} = {
        %{$report},
        pdf_id      => $relation->pdf_id(),
        status      => $relation->status(),
        name        => $c->loc($relation->name),
        timespan    => $c->loc( $relation->timespan() ),
        speakers    => \@speakers,
    };
}
=encoding utf8

=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
