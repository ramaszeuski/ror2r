package ROR2N::Controller::Users;
use Moose;
use namespace::autoclean;

use Data::Password qw(:all);

$DICTIONARY = 5;
$GROUPS     = 2;
$MINLEN     = 6;
$MAXLEN     = 32;

BEGIN { extends 'Catalyst::Controller'; }

=head1 NAME

ROR2N::Controller::Users - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
}

sub list :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $conditions = {
        deleted     => undef,
    };

    my @users = ();

    my $users = $c->model('DB::User')->search(
        $conditions,
        {
            order_by => ['login']
        }
    );

    USER:
    while ( my $user = $users->next() ) {

        push @users, {
            $user->get_columns,
        };
    }

    $c->stash(
        users      => \@users,
        no_wrapper => 1,
    );
}

sub edit :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $user;

    if ( $args->{id} ) {
        $user = $c->model('DB::User')->find(
            {
                id => $args->{id},
            }
        );

        if ( ! $user ) {
            $c->response->status(404);
            $c->response->body($c->loc("error.user.404"));
            return;
        }

        $c->stash->{user} = $user;
    }

    my @mask_roles = ();
    if ( $c->config->{hardware}{type} ne 'zdo' ) {
        @mask_roles = ('live', 'tests');
    }

    if ( $c->req->method eq 'GET') {
        my $roles;

        if ( $user ) {
            $roles = $user->roles_list({ mask => \@mask_roles })
        }
        else {
            $roles = $c->model('DB::User')->roles_list({ mask => \@mask_roles });
        }

        $c->stash(
            no_wrapper => 1,
            roles      => $roles,
        );
        return;
    }

    if ( ! $user ) {
            my $exists = $c->model('DB::User')->search(
                {
                    login   => $args->{login},
                    deleted => undef,
                },
            )->count;

            if ( $exists ) {
                $c->response->status(409);
                $c->response->body($c->loc("error.user.login.409"));
                return;
            }
    }

# Postizen uzivatele nemuzuo miy komplikovane heslo
#    if ( IsBadPassword( $args->{password} ) ) {
#        $c->response->status(412);
#        $c->response->body($c->loc("error.user.password.412"));
#        return;
#    }

    my $roles = $args->{'roles[]'};
    if ( ref $roles ) {
        $roles = join ' ', @{ $args->{'roles[]'} };
    }

    if ( $user ) {

        # neumoznime sobe sebrat spravu uzivatelu
        if ( $user->login eq $c->user->login && $roles !~ /\busers\b/) {
            $roles .= ' users';
        }

        $user->update({
            password => $args->{password},
            email    => $args->{email},
            phone    => $args->{phone},
            roles    => $roles,
        });
    }
    else {
        $c->model('DB::User')->create({
            login    => $args->{login},
            password => $args->{password},
            email    => $args->{email},
            phone    => $args->{phone},
            roles    => $roles,
        });
    }

    $c->stash(
        output => 'json',
        json   => {},
    );


}

sub delete :Local :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my $user = $c->model('DB::User')->find(
        {
            id => $args->{id},
        }
    );

    if ( $user->login eq $c->user->login) {
        $c->response->status(403);
        $c->response->body($c->loc("error.user.suicide"));
        return;
    }

    $user->update(
        { deleted => \'current_timestamp' },
     );

    $c->stash(
        output => 'json',
        json   => {},
    );
}
=head1 AUTHOR

Andrej Ramaseuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__
    if ( $args->{name} eq 'password' ) {
        if ( IsBadPassword( $args->{value} ) ) {
            $c->stash->{json} = {
                error => ERROR_BAD_PASSWORD,
            };
            return;
        }
    }
