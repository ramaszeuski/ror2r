package ROR2N::Model::At;

use strict;

use Moose;
extends 'Catalyst::Model';

use Schedule::At;
use Date::Manip;

use constant TAG => 'ROR2N';
use constant CMD => "/usr/bin/mosquitto_pub -t 'ror/cmd' -m 'broadcast %d'";

our $VERSION = '0.1';

Date_Init("DateFormat=non-US");

sub add {
    my ($self, $args) = @_;

    my $time = ParseDate( $args->{time} );

    my $rc = Schedule::At::add(
        TIME    => UnixDate ($time, '%Y%m%d%H%M'),
        COMMAND => sprintf(CMD, $args->{id} ),
        TAG     => TAG . ( $args->{id} // ''),
    );
}

sub delete {
    my ($self, $args) = @_;
    Schedule::At::remove(TAG => TAG . $args->{id});
}


__PACKAGE__->meta->make_immutable;

1;
