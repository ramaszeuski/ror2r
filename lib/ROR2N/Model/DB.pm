package ROR2N::Model::DB;
use Moose;
use namespace::autoclean;

use Date::Manip;

extends 'Catalyst::Model::DBIC::Schema';

our $VERSION = '0.01';
$VERSION = eval $VERSION;

__PACKAGE__->config(
    schema_class => 'ROR2N::Schema',
    connect_info => {
        dsn            =>  'dbi:SQLite:/var/lib/ror/ror2n.db',
        sqlite_unicode =>  1,
        quote_char     =>  q{"},
        name_sep       =>  q{.},
        on_connect_do  => [],
        sqlite_use_immediate_transaction => 1,
    }
);

=head1 NAME

ROR2::Model::DB - Catalyst DBIC Schema Model

=head1 DESCRIPTION

Catalyst Model.

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__

sub datatables_args {
    my $self  = shift;
    my $args  = shift;
    my $model = shift;

    my $mapping;
    if ( $model && $model->can('DATATABLES_MAPPING')) {
        $mapping = $model->DATATABLES_MAPPING();
    }

    my $conditions = {};

    my @columns = ();
    COLUMN:
    for my $n ( 0 .. $args->{iColumns} - 1 ) {
        my $column = $args->{'mDataProp_' . $n};

        push @columns, $column;

        my @search_columns;

        my $search = $args->{'sSearch_' . $n};

        if ( defined $search && length( $search) ) {

            $conditions->{$column} //= {
                ilike => '%' . $search . '%',
            };
        }

        next COLUMN if ! exists $args->{sSearch};
        next COLUMN if ! length( $args->{sSearch} );

        if ( $args->{'bSearchable_' . $n} eq 'true' ) {

            $conditions->{$column} //= {
                ilike => '%' . $args->{sSearch} . '%',
            };
        }
    }

    my @sorting;
    SORT_COLUMN:
    for my $n ( 0 .. $args->{iSortingCols} - 1 ) {
        next SORT_COLUMN if not exists $args->{"iSortCol_$n"};
        push @sorting, {'-'. $args->{"sSortDir_$n"} => $columns[$args->{"iSortCol_$n"}]};
    }

    return ( \@columns, \@sorting, $conditions );

}

sub init_filter {
    my $self   = shift;
    my $args   = shift;

    my $filter = $args->{filter};
    my $fields = $args->{filter_fields};
    my $form   = $args->{filter_form};

    FIELD:
    foreach my $field_name ( keys %{ $fields } ) {

        my $field = $fields->{$field_name};

        if ( ! exists $filter->{ $field_name } ) {
            if ( exists $field->{default} ) {
                if ( ref $field->{default} ) {
                    $filter->{$field_name} = &{ $field->{default} };
                }
                else {
                    $filter->{$field_name} = $field->{default};
                }
            }
            else {
                next FIELD;
            }
        }

        my $form_field = $form->get_field(
            {
                name => $field_name
            }
        ) // next FIELD;

        if ( $form_field->can('default')) {
            $form_field->default( $filter->{$field_name} );
        }
        if ( $form_field->can('value') ) {
            $form_field->value($filter->{$field_name});
        }
    }
    return $filter;
}

sub make_conditions_from_filter {
    my $self   = shift;
    my $args   = shift;

    my $model  = $args->{model};
    my $filter = $args->{filter};
    my $fields = $args->{filter_fields};

    my @where = ();

    ITEM:
    foreach my $name ( keys %{ $filter } ) {

        my $item = $fields->{$name};

        $item->{type} //= 'eq';

        next ITEM if $item->{type} eq 'custom';

        my $value = $filter->{ $name } // next ITEM;

        if ( exists $item->{regexp} && $value !~ $item->{regexp}) {
            next ITEM;
        }

        if ( $item->{type} eq 'ilike' ) {
            $value = { 'ilike' => "%$value%" };
        }
        elsif ( $item->{type} eq 'like' ) {
            $value = { 'like' => '%' . $value . '%' };
        }
        elsif ( $item->{type} =~ /timestamp_(begin|end)/ ) {

            my $bound = $1;
            my $time;

            if ( exists $item->{time} ) {
                if ( $filter->{$item->{time}} =~ /\d{1,2}:\d{1,2}/) {
                    $time = $filter->{$item->{time}};
                }
            }

            if ( $bound eq 'begin' ) {
                $value = ParseDate( "$value " . ( $time // '00:00') );
                $value &&= { '>=' => UnixDate($value, '%Y-%m-%d %H:%M:00')};
            }
            else {
                $value = ParseDate( "$value " . ( $time // '23:59') );
                $value &&= { '<=' => UnixDate($value, '%Y-%m-%d %H:%M:59')};
            }
        }
        elsif ( $item->{type} eq 'range' ) {

            next ITEM if $value !~ /(\d+)\D+(\d+)/;
            next ITEM if $1 > $2;

            my ( $min, $max ) = ( $1, $2 );
            my $multiplier = $item->{multiplier} || 1;
            my @range = ();

            $min = undef if exists $item->{min} && $min <= $item->{min};
            $max = undef if exists $item->{max} && $max >= $item->{max};

            push @range, { '>=' => $min * $multiplier } if defined $min;
            push @range, { '<=' => $max * $multiplier } if defined $max;

            $value = \@range;

        }

        my @columns;

        if ( exists $item->{columns} && ref $item->{columns} eq 'ARRAY' ) {
            @columns = @{$item->{columns}};
        }
        elsif ( exists $item->{column}) {
            @columns = ( $item->{column} );
        }
        else {
            @columns = ( $name );
        }

        @columns = grep { $model->result_source->has_column($_) } @columns;
        my @values = ( ref $value eq 'ARRAY') ? @{ $value } : ( $value );

        if ( scalar @columns > 1 ) {
            foreach my $value ( @values ) {
                push @where, [
                    map { { $_ => $value } } @columns
                ];
            }
        }
        elsif ( scalar @columns ) {
            foreach my $value ( @values ) {
                push @where, { $columns[0] => $value };
            }
        }

    }

    return @where;
}

