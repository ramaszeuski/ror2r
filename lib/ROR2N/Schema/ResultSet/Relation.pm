package ROR2N::Schema::ResultSet::Relation;

use strict;
use warnings;

use base 'DBIx::Class::ResultSet';

use constant ALERT_CONDITION => {
    properties => { like => '%"alert":1%' },
    report     => { '!=' => undef },
    deleted    => undef,
};

use Date::Manip;

sub collision {

    my $self = shift;
    my $args = shift;

    my $schema   = $self->result_source->schema;
    my $relation = $args->{relation} ? $args->{relation}->view() : undef;

    my $duration = $args->{duration} || ( $relation ? $relation->duration : 0 );
    my $id       = $relation ? $relation->id : undef;

    my $begin = $args->{begin} || UnixDate('now', '%Y-%m-%d %H:%M:%S');
    my $end   = UnixDate( DateCalc ($begin, "+$duration seconds"), '%Y-%m-%d %H:%M:%S');

    return $schema->resultset('Relation_view')->search(
        {
            id      => { '!=' => $id },
            deleted => undef,
            active  => 1,

            '-or' => [
                {
                    scheduled     => { '<=', $begin },
                    scheduled_end => { '>=', $begin },
                },
                {
                    scheduled     => { '<=', $end },
                    scheduled_end => { '>=', $end },
                },
                {
                    scheduled     => { '>=', $begin },
                    scheduled_end => { '<=', $end },
                },
            ]
        }
    )->first();


}

sub alerts_count {
    my $self = shift;
    return $self->count( ALERT_CONDITION );
}

sub alerts_list {
    my $self   = shift;

    my $relations = $self->search(
        ALERT_CONDITION,
        {
            columns  => [ qw(id name scheduled report) ],
            order_by => [{-desc => 'scheduled'}],
            rows     => 10,
        }
    );

    return $relations;
}



sub alerts_clear {
    my $self   = shift;

    my $conditions = ALERT_CONDITION;

# automaticky mazeme jen alerty relaci do vsech okruhu
#    $conditions->{channels} = { -not_like => '%general%' };

    my $relations = $self->search( ALERT_CONDITION );

    RELATION:
    while ( my $relation = $relations->next ) {
        my $properties = $relation->properties();
        delete $properties->{alert};
        $relation->update(
            {
                properties => $properties
            }
        );
    }
}

1;
