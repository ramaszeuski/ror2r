package ROR2N::Schema::ResultSet::Message;

use strict;
use warnings;

use base 'DBIx::Class::ResultSet';
use Time::HiRes qw(gettimeofday);

use constant SID_PREFIX => 'SID:';

our $VERSION = 1;

sub enqueue {
    my $self = shift;
    my $args = shift;

    return if ! $args->{recipients};
    return if ! $args->{subject};

    my $key = gettimeofday();

    my $schema = $self->result_source->schema;
    my @recipients = ();

    # defaultni scenar - vsechny uzivatele
    # TODO: uzivatele z urcitym opravnenim
    if ( $args->{ recipients } eq 'users' ) {

        # nebude to mit pokracovani
        $args->{ recipients } = [];

        my $users = $schema->resultset('User')->search(
            {
                active  => 1,
                deleted => undef,
            }
        );

        USER:
        while ( my $user = $users->next() ) {
            if ($user->email && $user->email =~ /@/) {
                push @recipients, {
                    address => $user->email,
                    method  => 'email',
                };
            }
        }
    }


    RECIPIENT:
    foreach my $recipient ( @{ $args->{recipients} } ) {

        @recipients = (
            @recipients,
            $self->inflate_recipient( $recipient ),
        );

    }

    MESSAGE:
    foreach my $recipient ( @recipients ) {

        # expirace ma vyznam jen u popup pro online uzivatele
#        my $expire = $recipient->{expire} || $args->{expire} || '1 day';

#        if ( $expire =~ /^\d+\s*(s|min|hour|day)/ ) {
#            $expire = \"now() + '$expire'::interval";
#        }
#        else {
#            $expire = undef;
#        }

        $schema->resultset('Message')->create(
            {
                key       => $key,
                sender    => $args->{sender},
                recipient => $recipient->{address},
                method    => $recipient->{method},
#                expire    => $expire,
                subject   => $args->{subject},
                content   => $args->{content},
                template  => $args->{template},
                options   => $args->{options},
            }
        )
    }

    return $key;
}

sub inflate_recipient {
    my $self = shift;
    my $recipient = shift;

    my ( $address, $method );

    if ( $recipient =~ s/^(email|notice|xmpp|sms|snmptrap):// ) {
        $address = $recipient;
        $method  = $1;
    }
    elsif ( $recipient =~ /@/ ) {
        ( $address, $method ) = ( $recipient, 'email' );
    }
    else {
        ( $address, $method ) = ( $recipient, 'notice' );
    }

    my @recipients = ();

    my $schema = $self->result_source->schema;

    if ( $address eq 'ONLINE' ) {
        my $online = $schema->resultset('Session')->search(
            { updated => { '>' => \"now() - '10 min'::interval" }}
        );
        SESSION:
        while ( my $session = $online->next ) {
            push @recipients, {
                address => SID_PREFIX . $session->id,
                method  => $method,
                expire  => '10 min',
            };
        }
    }
    else {
        push @recipients, {
            address => $address,
            method  => $method,
        };
    }

    return @recipients

}

sub dequeue {
    my $self = shift;
    my $args = shift;

    my $schema   = $self->result_source->schema;
    my @messages = ();

    my $conditions = {
#        expire   => { '>' => \'current_timestamp' },
        received => undef,
        deferred => undef,
        dequeued => undef,
    };

    # pro web! vzdy vytahujeme jen cilene
    if ( $args->{KEY} ) {
        $conditions->{method} = ['notice', 'modal'];

        my $session = $schema->resultset('Session')->find(
            {
                id => $args->{KEY}
            }
        ) || return undef;

        $conditions->{recipient} = [
            $session->account_login,
            SID_PREFIX . $args->{KEY},
        ];

    }
    else {
        $conditions->{method} = $args->{method} || ['email', 'jabber'];
        $conditions->{sended} = undef;
    }

    my $attributes = {
        order_by => ['counter', 'enqueued'],
    };

    if ( $args->{count} ) {
        $attributes->{rows} = $args->{count};
    };

    my $messages = $schema->resultset('Message')->search(
        $conditions,
        $attributes,
    );

    return $messages;

}

1;

