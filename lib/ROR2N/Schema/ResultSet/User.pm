package ROR2N::Schema::ResultSet::User;

use strict;
use warnings;

use base 'DBIx::Class::ResultSet';

use constant ROLES => [
    qw(
        live
        volume
        library
        relations
        relations_archive
        speakers
        speakers_edit
        tests
        users
        settings
        reboot
    )
];

sub roles_list {
    my $class = shift;
    my $args  = shift;

    my %mask = ();

    if ( $args->{mask} && ref $args->{mask} eq 'ARRAY') {
        %mask = map { $_ => 1 } @{$args->{mask}};
    }

    my @roles = ();

    ROLE:
    foreach my $role ( @{ ROLES() } ) {
        next ROLE if $mask{ $role };
        push @roles, { id => $role };
    }

    return \@roles;
}

1;
