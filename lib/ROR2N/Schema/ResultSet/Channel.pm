package ROR2N::Schema::ResultSet::Channel;

use strict;
use warnings;

use base 'DBIx::Class::ResultSet';

sub list {
    my $self = shift;
    my $args = shift;

    my $schema     = $self->result_source->schema;
    my $conditions = {};


    if ( $args->{not_empty} ) {
        my $not_empty = $schema->resultset('Speaker')->search(
            {
                active   => 1,
            },
            {
                columns  => ['channel_id'],
                distinct => 1,
            }
        );

        ID:
        while ( my $channel = $not_empty->next() ) {
            push @{ $conditions->{id} }, $channel->channel_id();
        }

        if ( ! exists $conditions->{id} ) {
            return ();
        }

    }

    my $channels = $schema->resultset('Channel')->search(
        $conditions,
        {
            columns => ['id', 'name'],
            order_by => 'id'
        }
    );

    my @channels = ();

    CHANNEL:
    while ( my $channel = $channels->next() ) {
        push @channels, { $channel->get_columns() };
    }

    return @channels;

}


1;


