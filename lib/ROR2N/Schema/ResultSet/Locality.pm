package ROR2N::Schema::ResultSet::Locality;

use strict;
use warnings;

use base 'DBIx::Class::ResultSet';

sub list {
    my $self = shift;

    my $schema   = $self->result_source->schema;

    my $localities = $schema->resultset('Locality')->search(
        { active => 1 },
        {
            columns => ['id', 'name'],
            order_by => 'name'
        }
    );

    my @localities = ();

    LOCALITY:
    while ( my $locality = $localities->next() ) {
        push @localities, { $locality->get_columns() };
    }

    return @localities;

}

1;

