package ROR2N::Schema::ResultSet::Speaker;

use strict;
use warnings;

use base 'DBIx::Class::ResultSet';

use Date::Manip;

use constant TYPES  => [ 1, 2, 3, 4 ];
use constant INPUTS => {
    1 => [],
    2 => [
        {
            id => 'a3',
            class => 'number',
            formatter => sub {
                my $value = shift;
                return '-' if $value !~ /^\d+$/;
                return sprintf('%5.2fV', $value * 0.017884914);
            }

        },
        { id => 'a1' },
        { id => 'a2' },
    ],
    3 => [
        {
            id    => 'opened',
            alert => sub {
                my $value = shift;
                return $value;
            }
        },
        {
            id    => 'battery',
            alert => sub {
                my $value = shift;
                return 0 if ! $value;
                return ( $value >= 2 ) ? 0 : 1;
            }
        },
        {
            id => 'a3',
#            class => 'number',
            formatter => sub {
                my $value = shift;
                return '-' if $value !~ /^\d+$/;
                return sprintf('%5.2fV', $value * 0.017884914);
            }

        },
        { id => 'charging' },
#        { id => 'amplifier' },
        {
            id        => 'volume1',
            type      => 'volume',
            formatter => sub {
                my $value = shift;
                return '-' if $value !~ /^\d+$/;
                return sprintf('%3d%%', $value * 100 / 254);
            }
        },
        {
            id        => 'volume2',
            type      => 'volume',
            formatter => sub {
                my $value = shift;
                return '-' if $value !~ /^\d+$/;
                return sprintf('%3d%%', $value * 100 / 254);
            }
        },
#        { id => 'a1' },
#        { id => 'a2' },
    ],
    4 => [],
};

use constant REQUIRED_EVENTS => {
    3 => [ qw/on audio/ ]
};

sub free_ids {
    my $self = shift;
    my $args = shift;

    my %id = map { $_ => 1 } ( 1 .. 255 );

    my $schema   = $self->result_source->schema;

    my $exists = $schema->resultset('Speaker')->search(
        {},
        { columns => ['id']}
    );

    EXISTS:
    while ( my $speaker = $exists->next() ) {
        delete $id{ $speaker->id };
    }

    return keys %id;
}

sub types {
    return TYPES();
}

sub inputs {
    my $self = shift;
    my $type = shift;
    return INPUTS->{$type};
}

sub required_events {
    my $self = shift;
    my $type = shift;
    return REQUIRED_EVENTS->{$type};
}

1;
