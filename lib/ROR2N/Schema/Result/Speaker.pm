package ROR2N::Schema::Result::Speaker;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;

our $VERSION = 1;

__PACKAGE__->table('speakers');

__PACKAGE__->add_columns(
    qw(
        id
        type
        active
        status

        name
        description

        locality_id
        channel_id

        gps
        input_names
        last_check
        last_data
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    locality => 'ROR2N::Schema::Result::Locality',
    {
        'foreign.id' => 'self.locality_id',
    },
    { join_type => 'left' },
);

__PACKAGE__->belongs_to(
    channel => 'ROR2N::Schema::Result::Channel',
    {
        'foreign.id' => 'self.channel_id',
    },
);

__PACKAGE__->has_many(
    logs => 'ROR2N::Schema::Result::Speaker_Log',
    {
        'foreign.speaker_id' => 'self.id'
    },
);

__PACKAGE__->inflate_column('input_names', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});

__PACKAGE__->inflate_column('last_data', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});

sub inputs {
    my $self = shift;
    return $self->resultset_class->inputs( $self->type );
}

sub required_events {
    my $self = shift;
    return $self->resultset_class->required_events( $self->type );
}

sub lat {
    my $self = shift;
    return if ! $self->gps;
    if ( $self->gps =~ /([\d\.]+)[,\s]*([\d\.]+)/ ) {
        return $1 + 0;
    }
}

sub lng {
    my $self = shift;
    return if ! $self->gps;
    if ( $self->gps =~ /([\d\.]+)[,\s]*([\d\.]+)/ ) {
        return $2 + 0;
    }
}

sub full_name {
    my $self = shift;
    my $name = $self->name;
    if ( $self->locality_id ) {
        $name .= ', ' . $self->locality->name;
    }
    return $name;
}


1;
