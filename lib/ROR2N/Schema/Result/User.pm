package ROR2N::Schema::Result::User;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('user');

__PACKAGE__->add_columns(
    id => {
        data_type         => 'integer',
        is_auto_increment => 1,
        is_nullable       => 0,
    },
    qw(
        active
        created
        deleted
        login
        password
        name
        firstname
        lastname
        email
        messenger
        phone
        cellular
        fax
        country
        region
        city
        address
        zip
        profile
        notes
        roles
    ),
);

__PACKAGE__->set_primary_key('id');

#sub name {
#    my $self = shift;
#    my $name =
#        join ' ',
#        grep /\w/,
#        (
#            $self->firstname // '',
#            $self->lastname // '',
#        );
#
#    return $name || $self->login;
#
#}

sub is_deletable {
    my $self = shift;
    return 1;
}

sub roles_list {
    my $self = shift;
    my $args = shift;

    my @roles   = ();
    my %mask    = ();
    my %granted = map { $_ => 1 } split /\W+/, $self->roles;

    if ( $args->{mask} && ref $args->{mask} eq 'ARRAY') {
        %mask = map { $_ => 1 } @{$args->{mask}};
    }

    ROLE:
    foreach my $role ( @{ $self->result_source->resultset_class->ROLES() } ) {
        next ROLE if $mask{ $role }; #TODO: jako varianta - pridat attribut masked
        push @roles, {
            id      => $role,
            granted => $granted{ $role },
        };
    }

    return \@roles;
}

1;

__END__

__PACKAGE__->has_many(
    log_events => 'ROR2::Schema::Result::Log',
    {
        'foreign.user_id' => 'self.id'
    },
);


