package ROR2N::Schema::Result::Record;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;

our $VERSION = 1;

__PACKAGE__->table('records');

__PACKAGE__->add_columns(
    qw(
        id

        category_id
        active
        priority

        created
        deleted

        duration
        name
        description
        properties

    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    playlist_items => 'ROR2N::Schema::Result::Playlist',
    {
        'foreign.record_id' => 'self.id'
    },
);

__PACKAGE__->inflate_column('properties', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});

sub duration_hms {
    my $self = shift;
    return $self->result_source->schema->hms( $self->duration );
}


1;

