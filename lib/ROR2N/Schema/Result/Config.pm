package ROR2N::Schema::Result::Config;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('config');

__PACKAGE__->add_columns(
    qw(
        id
        name
        description
        value
    ),
);

__PACKAGE__->set_primary_key('id');

1;

