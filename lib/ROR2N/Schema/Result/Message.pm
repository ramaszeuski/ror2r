package ROR2N::Schema::Result::Message;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('messages');
__PACKAGE__->add_columns(
    id => {
        is_auto_increment => 1,
        data_type         => 'integer',
        is_nullable       => 0,
    },
    qw(
        key

        enqueued
        sended
        received
        deferred
        dequeued
        expire
        counter

        sender
        recipient
        method
        subject
        content
        template
        options
    )
);
__PACKAGE__->set_primary_key('id');

__PACKAGE__->might_have(
    'recipient_user',
    'ROR2N::Schema::Result::User',
    {
       'foreign.login'  => 'self.recipient',
    }
);

__PACKAGE__->has_many(
    'siblings',
    'ROR2N::Schema::Result::Message',
    {
        'foreign.key'    => 'self.key',
        'foreign.method' => 'self.method',
    },
    {
        cascade_delete => 0
    },
);

__PACKAGE__->inflate_column('content', {
    deflate => sub {
        my $content = shift;

        if ( ref $content ) {
            $content = to_json( $content );
        }
        return $content;
    },
    inflate => sub {
        my $content = shift;
        my $message = shift;

        if ( $content =~ /^{/ ) {
            $content = from_json( $content );
        }

        return $content;

    },
});

__PACKAGE__->inflate_column('options', {
    deflate => sub { to_json( shift );  },
    inflate => sub { from_json( shift );  },
});

1;

__END__






