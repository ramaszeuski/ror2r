package ROR2N::Schema::Result::Relation_view;

use strict;
use warnings;
use Date::Manip;

use base 'ROR2N::Schema::Result::Relation';

our $VERSION = 1;

__PACKAGE__->table('relations_view');

__PACKAGE__->add_columns(
    qw(
        scheduled_end
    ),
);

sub scheduled_end_time {
    my $self = shift;
    return '' if ! $self->scheduled_end;
    return UnixDate( $self->scheduled_end, '%H:%M');
}

1;

