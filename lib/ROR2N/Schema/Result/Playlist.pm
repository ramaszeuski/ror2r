package ROR2N::Schema::Result::Playlist;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('playlist');

__PACKAGE__->add_columns(
    qw(
        id
        relation_id
        record_id
        position
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    relation => 'ROR2N::Schema::Result::Relation',
    {
        'foreign.id' => 'self.relation_id',
    },
);

__PACKAGE__->belongs_to(
    record => 'ROR2N::Schema::Result::Record',
    {
        'foreign.id' => 'self.record_id',
    },
);

1;
