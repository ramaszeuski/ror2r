package ROR2N::Schema::Result::Playlist_view;

use strict;
use warnings;

use base 'ROR2N::Schema::Result::Playlist';

our $VERSION = 1;

__PACKAGE__->table('playlist_view');

__PACKAGE__->add_columns(
    qw(
        category_id
        record_name
        record_duration
    ),
);

__PACKAGE__->set_primary_key('id');

sub record_duration_hms {
    my $self = shift;
    return $self->result_source->schema->hms( $self->record_duration );
}
1;


