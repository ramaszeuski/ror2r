package ROR2N::Schema::Result::Relation;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use Date::Manip;
use JSON;

our $VERSION = 1;

use constant STATUS_WARN  => 1;
use constant STATUS_ERROR => 100;

use constant DATETIME_FORMAT => '%s %s-%s';

__PACKAGE__->table('relations');

__PACKAGE__->add_columns(
    id => {
        is_auto_increment => 1,
    },
    qw(
        active

        created
        deleted
        scheduled
        broadcasted

        duration
        channels

        name
        description
        properties
        report

    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_one(
    view => 'ROR2N::Schema::Result::Relation_view',
    {
        'foreign.id' => 'self.id'
    },
);

__PACKAGE__->has_many(
    playlist_items => 'ROR2N::Schema::Result::Playlist',
    {
        'foreign.relation_id' => 'self.id'
    },
);

__PACKAGE__->has_many(
    playlist_view => 'ROR2N::Schema::Result::Playlist_view',
    {
        'foreign.relation_id' => 'self.id'
    },
);

use constant SPEAKER_EVENT => {
     8 => 'on',
    18 => 'audio',
};


__PACKAGE__->inflate_column('properties', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});

__PACKAGE__->inflate_column('report', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});


sub has_intro {
    my $self = shift;
    my $id   = shift;

    my $properties = $self->properties;
    return 0 if ! exists $properties->{intros};

    TYPE:
    foreach my $type ('begining', 'ending') {
        my $intros = $properties->{intros}{$type};
        next TYPE if ! $intros;
        next TYPE if ! ref $intros;
        INTRO:
        foreach my $intro ( @{ $intros} ) {
            next INTRO if ! defined $intro;
            return 1 if $intro eq $id;
        }
    }

    return 0;
}

sub playlist {
    my $self = shift;

    my @playlist;
    my $properties = $self->properties;

    if ( $properties->{intros} && $properties->{intros}{begining} ) {
        @playlist = grep(defined,  @{ $properties->{intros}{begining} });
    }

    my $items = $self->playlist_items(
        {  },
        { order_by => ['position'] },
    );

    RECORD:
    while ( my $item = $items->next ) {
        push @playlist, $item->record_id;
    }

    return @playlist;
}

sub ending {
    my $self = shift;

    my @playlist;
    my $properties = $self->properties;

    if ( $properties->{intros} && $properties->{intros}{ending} ) {
        @playlist = grep(defined,  @{ $properties->{intros}{ending} });
    }

    return @playlist;
}

sub add_channel {
    my $self    = shift;
    my $channel = shift;

    my %channels = map {$_ => 1} split ' ', ($self->channels // '');
    $channels{$channel} = 1;
    $self->update({ channels => join ' ', sort keys %channels });
}

sub del_channel {
    my $self    = shift;
    my $channel = shift;

    my %channels = map {$_ => 1} split ' ', ($self->channels // '');
    delete $channels{$channel};
    $self->update({ channels => join ' ', sort keys %channels });
}

sub configured {
    my $self = shift;
    return $self->duration && $self->channels;
}

sub obsolete {
    my $self = shift;
    return 0 if ! $self->scheduled;
    return Date_Cmp( $self->scheduled, 'now' ) <= 0;
}

sub status {
    my $self = shift;
    if ( $self->report
        && exists $self->report->{status_id}
        && $self->report->{status_id}=~/^\d+$/
    ) {
        return 'error' if $self->report->{status_id} >= STATUS_ERROR;
        return 'warn'  if $self->report->{status_id} >= STATUS_WARN;
        return 'broadcasted';
    }
    else {
        return 'inactive' if ! $self->active;
        return 'inactive' if ! $self->scheduled;
        return 'inactive' if $self->obsolete;
        return 'unconfigured' if ! $self->configured;
        return 'ready';
    }
}

sub testing {
    my $self = shift;
    return 0 if ! $self->properties;
    return 0 if ! ref $self->properties;
    return 0 if ! exists $self->properties->{test};
    return $self->properties->{test};
}

sub duration_hms {
    my $self = shift;
    return $self->result_source->schema->hms( $self->duration );
}

sub scheduled_day {
    my $self = shift;
    return '' if ! $self->scheduled;
    return UnixDate( $self->scheduled, '%d.%m.%Y');
}

sub scheduled_time {
    my $self = shift;
    return '' if ! $self->scheduled;
    return UnixDate( $self->scheduled, '%H:%M');
}

sub pdf_id {
    my $self = shift;

    return UnixDate( $self->scheduled, 'ror2report-%Y%m%d-%H%M%S');
}

sub timespan {
    # TODO: unifikace, simplifikace
    my $self = shift;

    return 'not.scheduled' if ! $self->scheduled;

    if ( $self->broadcasted ) {
#        return 'not.broadcasted' if $self->report
#                     && $self->report->{status_id} >= STATUS_ERROR;
        return
            UnixDate( $self->scheduled, '%d.%m.%Y %H:%M')
            . '-' .
            UnixDate( $self->broadcasted, '%H:%M')
    }
    else {
        return UnixDate($self->scheduled, '%d.%m.%Y %H:%M');
#         return sprintf( DATETIME_FORMAT,
#                $self->scheduled_day,
#                $self->scheduled_time,
#                $self->scheduled_end_time,
#         );
    }
}

sub make_report {
    my $self = shift;
    my $args = shift;

    my $properties = $self->properties;
    my $schema = $self->result_source->schema;

    my $report = {
        channels  => [],
        record_id => $args->{record_id} // '',
        status_id => $args->{status_id} // '',
    };

    # Staticky seznam okruhu a hlasicu
    my $speakers_filter = {
        active => 1,
    };

    if ( $self->channels =~ /general/ ) {
        $report->{channels} = [ { id => 'general'} ];
    }
    else {
        my $channel_ids = [ split /\D+/, $self->channels ];

        my $channels = $schema->resultset('Channel')->search(
            {
                id => { -in => $channel_ids }
            },
            {
                columns => [ qw(id name) ]
            }
        );
        while ( my $channel = $channels->next() ) {
            push @{ $report->{channels} }, {
                $channel->get_columns()
            };
        }

        $speakers_filter->{channel_id} = { -in => $channel_ids };
    }

    my $speakers = $schema->resultset('Speaker')->search(
        $speakers_filter,
    );

    while ( my $speaker = $speakers->next() ) {
        $report->{speakers}{ $speaker->id } = {
            type            => $speaker->type,
            name            => $speaker->name,
            channel_id      => $speaker->channel_id,
            locality_id     => $speaker->locality_id,
            required_events => $speaker->required_events(),
            on              => undef,
            audio           => undef,
            off             => undef,
            volume          => undef,
            battery         => undef,
            errors          => 0,
        };
    }

    my $records;

    if ( $self->properties->{live} ) {
        # zive vysilani
        my @begining = ();
        my @ending   = ();

        if ( $properties->{intros} && $properties->{intros}{begining} ) {
            @begining = grep(defined,  @{ $properties->{intros}{begining} });
        }
        if ( $properties->{intros} && $properties->{intros}{ending} ) {
            @ending = grep(defined,  @{ $properties->{intros}{ending} });
        }

        # ziskame vsechny data o znelkach z databazi
        my %intros = ();

        my $playlist = $schema->resultset('Record')->search(
            {
                id => {'-in' => [@begining, @ending] }
            },
            {
                columns => [qw(id duration name)]
            }
        );

        RECORD:
        while ( my $record = $playlist->next ) {
            $intros{ $record->id } = { $record->get_columns };
        }

        $intros{live} = {
            id       => 'live',
            duration => $report->{duration},
            name     => 'Live',
        };

        @{ $report->{playlist}} = map { $intros{$_} } (
            @begining,
            'live',
            @ending,
        );

        # v reportu nemaji co delat
        delete $properties->{intros};
        delete $properties->{live};

        # cely zaznam do playlist?

    }
    else {
        # bezne vysilani
        $records = $self->playlist_view(
            { },
            {
                order_by => 'position',
                columns  => [ qw(record_id record_duration record_name) ],
            },
        );

        RECORD:
        while ( my $item = $records->next ) {
            push @{ $report->{playlist}}, {
                id       => $item->record_id,
                duration => $item->record_duration,
                name     => $item->record_name,
            };
        }

    }

    if ( $self->broadcasted ) {
        # data z hlasicu
        my $speakers_log = $schema->resultset('Speaker_Log')->search(
            {
                time => {
                    '>=' =>  $self->scheduled,   #TODO: live
                    '<=' =>  $self->broadcasted, #TODO: primo tady?
                },
                speaker_id => {
                    -in => [
                        keys %{ $report->{speakers} },
                    ],
                }
            }
        );

        LOG:
        while ( my $log = $speakers_log->next() ) {

            my $id    = $log->speaker_id;
            my $event = $log->data->{event};

            $report->{speakers}{ $id }{volume}  //= [ $log->data->{volume1}, $log->data->{volume2}, ];
            $report->{speakers}{ $id }{battery} //= $log->data->{battery};

            if ( exists SPEAKER_EVENT->{$event} ) {
                $report->{speakers}{ $id }{SPEAKER_EVENT->{$event}} = $log->time;
            }
        }

        SPEAKER:
        foreach my $speaker_id ( keys %{$report->{speakers}} ) {
            my $speaker = $report->{speakers}{$speaker_id};

            my $required_events = $speaker->{required_events};
            my $inputs = $schema->resultset('Speaker')->inputs( $speaker->{type} );

            next SPEAKER if ! ref $required_events;

            EVENT:
            foreach my $event ( @{$required_events} ) {
                if ( ! $speaker->{ $event } ) {
                    $speaker->{ $event } = 'NO';
                    $speaker->{errors}++;
                    $report->{status_id} = 1;
                }
                # u obousmeru pri neznmeme stavu baterie
                if ( $event eq 'on' ) {
                    $speaker->{ battery } //= 4;
                }
            }

            INPUT:
            foreach my $input ( @{$inputs} ) {
                if ( ref $input->{alert} ) {
                    if ($input->{ alert }( $speaker->{$input} )) {
                        $speaker->{errors}++;
                        $report->{status_id} = 1;
                    }
                }
            }
            $report->{speakers}{$speaker_id} = $speaker;
        }
    } #broadcasted

    if ( $report->{status_id} ) {
        $properties->{alert} = 1;
        # vytvorit message!
    }

    $schema->resultset('Message')->enqueue(
        {
            recipients => 'users',
            subject    => 'REPORT',
            content    => {
                relation_id => $self->id,
            },
            template   => 'relation_report_' . ($report->{status_id} || 0),
        }
    );

    $self->update(
        {
            report     => $report,
            properties => $properties,
        }
    );

    return $report;
}

1;
