package ROR2N::Schema::Result::Locality;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('localities');

__PACKAGE__->add_columns(
    qw(
        id
        active
        name
        description
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    speakers => 'ROR2N::Schema::Result::Speaker',
    {
        'foreign.locality_id' => 'self.id'
    },
);

1;
