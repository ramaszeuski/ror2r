package ROR2N::Schema::Result::Channel;

use strict;
use warnings;

use base 'DBIx::Class::Core';

our $VERSION = 1;

__PACKAGE__->table('channels');

__PACKAGE__->add_columns(
    qw(
        id
        active
        name
        description
    ),
);

__PACKAGE__->set_primary_key('id');

1;
