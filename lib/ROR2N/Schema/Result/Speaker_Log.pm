package ROR2N::Schema::Result::Speaker_Log;

use strict;
use warnings;

use base 'DBIx::Class::Core';
use JSON;

our $VERSION = 1;

__PACKAGE__->table('speakers_log');

__PACKAGE__->add_columns(
    qw(
        id
        speaker_id
        speaker_type
        time
        data
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    speaker => 'ROR2N::Schema::Result::Speaker',
    {
        'foreign.id' => 'self.speaker_id',
    },
);

__PACKAGE__->inflate_column('data', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});
1;

__END__
