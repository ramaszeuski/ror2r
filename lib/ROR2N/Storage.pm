package ROR2N::Storage;

use strict;
use warnings;
use File::Path qw(make_path);
use File::Copy;
use File::MimeInfo;
use File::Find;
use Digest::SHA;
use MP3::Info;
use Date::Manip;
use Audio::MPD;

our $VERSION = '0.02';

use constant LAME            => '/usr/bin/lame';
use constant LAME_OPTIONS    => [ '-S' ];
use constant MPLAYER         => '/usr/bin/mplayer';
use constant MPLAYER_OPTIONS => [ '-ao' ];

sub new {
    my $classname = shift;
    my $path      = shift || die "Storage directory not defined";
    my $tmp       = shift || "$path/../tmp";

    my $self      = {};

    -r $path || make_path( $path ) || die "Can't create storage directory $path\n";
    -r $tmp  || make_path( $tmp );

    $self->{path} = $path;
    $self->{tmp}  = $tmp;

    bless ($self, $classname);
    return $self;
}

sub store {
    my $self = shift;
    my $file = shift // return;
    my $args = shift;

    my $id         = Digest::SHA->new->addfile( $file, 'b')->hexdigest();
    my $fullname   = $self->{path} . "/$id.mp3";
    my %properties = ( mime => mimetype( $file ) );

    eval {
        if ( $properties{mime} !~ /audio\/(mpeg|mp3)/) {
            if ( $properties{mime} =~ /audio/ ) {
                my $rc = 0;

                if ( $properties{mime} =~ /wav$/ ) {
                    $rc  = system(LAME, @{ LAME_OPTIONS() }, $file, $fullname);
                }
                else {
                    my $pcm = $self->{tmp} . "/$id.wav";
                    $rc  = system(MPLAYER, @{ MPLAYER_OPTIONS() }, "pcm:file=$pcm", $file);
                    if ( ! $rc ) {
                        $rc  = system(LAME, @{ LAME_OPTIONS() }, $pcm, $fullname);
                        unlink $pcm;
                    }
                }

                die "Can't encode" if $rc;

                unlink( $file ) if ! $args->{copy};
            }
            else {
                die "Unsupported type";
            }
        }
        else {

            if ( $args->{copy} ) {
                copy( $file, $fullname );
            }
            else {
                move( $file, $fullname );
            }

            if ( my $mp3tag   = get_mp3tag(  $fullname ) ) {
                %properties = ( %properties,
                    tracknum => $mp3tag->{TRACKNUM},
                    artist   => $mp3tag->{ARTIST},
                    album    => $mp3tag->{ALBUM},
                    title    => $mp3tag->{TITLE},
                );
            }
        }
    };

    return { error => $@ } if $@;

    my $mpd = Audio::MPD->new();
    chmod 0644, $fullname;
    $mpd->updatedb;


    PROPERTY:
    foreach my $property ( keys %properties ) {
        delete $properties{ $property } if ! $properties{ $property };
    }

    my $mp3info  = get_mp3info( $fullname );

    return {
        id        => $id,
        duration   => int($mp3info->{SECS}),
        properties => \%properties,
    };

}

sub store_dir {
    my $self = shift;
    my $dir  = shift // return;
    my $args = shift;

    my @records;

    find (
        sub {
            return if ! -f $File::Find::name;
            my $record = $self->store(
                $File::Find::name,
                $args,
            );
            push @records, $record if $record->{id};
        },
        $dir
    );

    return @records;
}

sub get {
    my $self = shift;
    my $id   = shift // return;
    my $file = $self->{path} . "/$id.mp3";

    if ( -f $file ) {
        return $file;
    }
}

sub delete {
    my $self = shift;
    my $id   = shift;
    my $hard = shift;

    my $file = $self->{path} . "/$id.mp3";

    if ( $hard ) {
        unlink( $file );
    }
    else {
        my $timestamp = UnixDate('now', '%Y%m%d%H%M%S');
        rename( $file, "$file.D$timestamp");
    }
}

sub all {
    my $self = shift;

    my @records = ();

    opendir STORAGE, $self->{path};

    RECORD:
    foreach my $record ( readdir STORAGE ) {
        next RECORD if $record !~ /^(\w+)\.mp3$/;
        push @records, $1;
    }

    closedir STORAGE;

    return @records;
}

sub purge {
    my $self = shift;

    opendir STORAGE, $self->{path};

    RECORD:
    foreach my $record ( readdir STORAGE ) {
        next RECORD if $record !~ /(\w+)\.mp3\.D\d+/;
        unlink $self->{path} . "/$record";
    }

    closedir STORAGE;
}

__END__
