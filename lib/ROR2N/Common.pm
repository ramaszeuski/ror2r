package ROR2N::Common;

use strict;
use warnings;
use FindBin qw($Bin);

use constant MQTT_PREFIX   => 'ror/';
use constant MQTT_PUBLISH  => q{mosquitto_pub -t '%s' -m '%s' -q %d};
use constant MQTT_RETAIN   => q{mosquitto_pub -t '%s' -m '%s' -q %d -r};
use constant PUBLUSH_DELAY => 50000;
use constant CONFIG_FILES  => [
    "$Bin/../ror2n.yaml",
    '/var/lib/ror/ror2n.yaml',
    '/boot/ror/ror.ini',
];

use constant OPTIONS  => [
  [ 'mqtt|m:s',        'MQTT server', { required => 1, default => 'localhost'} ],
  [ 'log-screen:s',    'Loglevel pro obrazovku' ],
  [ 'log-syslog:s',    'Loglevel pro syslog' ],
  [ 'erase-shm',       'Uvolnit sdilenou pamet'],
  [ 'help|h',          'Tato napověda' ],
];

use Getopt::Long::Descriptive;
use Config::Any;
use Net::MQTT::Simple;
use Audio::MPD;
use Time::HiRes qw(usleep);
use Redis;
use JSON;

use ROR2N::Schema;
use ROR2N::Storage;
use ROR2N::Tools::Logger;
use ROR2N::Tools::PulseAudio;
use ROR2N::Hardware::RPi;
use ROR2N::Hardware::ZDO;

BEGIN {
    require Exporter;
    our $VERSION = 1.00;
    our @ISA     = qw(Exporter);
    our @EXPORT  = qw(
        %shm
        @threads
        $opt
        $cfg
        $logger
        $mqtt
        $mpd
        $db
        $storage
        $rpi
        $zdo
        $pa

        rpi
        mic
        gpio
        shm
        trace
        error
        enqueue
    ); # neexportovat $logger %shm !

}

use vars qw(
    @threads
    $opt
    $cfg
    $db
    $storage
    %shm
    $logger
    $mqtt
    $mpd
    $rpi
    $zdo
    $pa
    $redis
);

################
# INICIALIZACE #
################

sub init {
    my %args = @_;

    my @options = @{ (OPTIONS) };

    if ( exists $args{options} && ref $args{options} ) {
        @options = ( @{ $args{options} }, @options );
    }

    # parametry prikazove radky
    ( $opt, my $usage ) = describe_options( '%c %o', @options );
    $usage->die() if $opt->help;

    if ( $opt->erase_shm ) {
        %shm = ();
    }

    # nacteni konfiguracnich souboru
    my $configs = Config::Any->load_files( { files => CONFIG_FILES, flatten_to_hash => 0, use_ext => 1 } );

    # defaultni konfigurace
    $cfg = {};

    # spojeni konfiguracnich souboru
    CONFIG:
    foreach my $config_file ( @{ CONFIG_FILES() }) {
        KEY:
        foreach my $key ( keys %{ $configs->{ $config_file }}  ) {
            # VALIDACE!
            $cfg->{ $key } = $configs->{ $config_file }{ $key };
        }
    }

    # inicializace redis
    $redis = Redis->new(
        reconnect              => 5,
        every                  => 5000,
        conservative_reconnect => 1,
    );

    # inicializace spojeni z databazi
    $db = ROR2N::Schema->connect({
        sqlite_unicode =>  1,
        quote_char     =>  q{"},
        name_sep       =>  q{.},
        on_connect_do  => [],
        sqlite_use_immediate_transaction => 1,
        %{ $cfg->{'Model::DB'}{connect_info} },
    });

    # nacteni casti configurace z databazi
    DB_CONFIG:
    foreach my $db_config ( $db->resultset('Config')->all() ) {
        $cfg->{ $db_config->id } = from_json( $db_config->value );
    }

    $logger = ROR2N::Tools::Logger->new(
        {
            screen => $opt->log_screen // $cfg->{log}{screen},
            syslog => $opt->log_syslog // $cfg->{log}{syslog},
            syslog_ident => $args{syslog_ident} // $0,
        }
    );
    trace('Starting');

    $rpi     = ROR2N::Hardware::RPi->new();
    $mqtt    = Net::MQTT::Simple->new( $opt->mqtt );
    $storage = ROR2N::Storage->new( $cfg->{storage}{path} );

    if ( $args{zdo} && $cfg->{hardware}{type} eq 'zdo' ) {
        $zdo = ROR2N::Hardware::ZDO->new({
            device    => $cfg->{zdo}{device},
            stopbits  => rpi() ? 1 : 2,
            handshake => rpi() ?  'rts' : 'none',
        });
        $zdo->plugins();
    }

    if ( $args{mpd} ) {
        $mpd = Audio::MPD->new($cfg->{mpd} // {});
    }

    if ( $args{pa} ) {
        $pa = ROR2N::Tools::PulseAudio->new( $cfg->{pulseaudio} );
    }

}

sub trace {
    $logger->trace( @_ );
}

sub enqueue {

    my $mode = 'retain';
    my $qos  = 0;

    if ( ! ref $_[0] ) {
        $mode = shift;
    }

    if ( ! ref $_[0] ) {
        $qos = shift;
    }

    RESPONSE:
    foreach my $msg ( @_ ) {
        next RESPONSE if ! defined $msg;
        next RESPONSE if ! ref $msg;

        my ( $topic, $value ) = @{ $msg };
        my $shm_key = $topic;
        $shm_key =~ s/\//_/g;
        $topic = MQTT_PREFIX . $topic;

        if ( $mode !~ /nolog/ ) {
            $logger->trace("Enqueue ($mode,$qos) $topic: $value");
        }

        if ( $mode =~ /retain/ ) {
            if ( $qos ) {
                system( sprintf(MQTT_RETAIN, $topic, $value, $qos ));
            }
            else {
                $mqtt->retain( $topic => $value );
            }

            shm($shm_key => $value);
        }
        else {
            if ( $qos ) {
                system( sprintf(MQTT_PUBLISH, $topic, $value, $qos ));
            }
            else {
                $mqtt->publish( $topic => $value );
            }

        }

        usleep(PUBLUSH_DELAY);
    }
}

sub error {
    my $error = shift // return;
    trace("ERROR: $error", 'error');
    enqueue('publish', [ 'error' => $error] );
}

sub shm {
    my $key   = shift;

    # WRITE/DELETE
    if ( scalar @_ ) {
        my $value = shift;

        if ( defined $value ) {
            $redis->set( $key => $value );
        }
        else {
            $redis->del( $key );
        }
    }
    # READ
    else {
        return $redis->get($key);
    }

}

sub rpi {
    if ( $rpi && $rpi->{compatible} ) {
        return 1;
    }
}

sub gpio {
    return $rpi->gpio(@_);
}

sub mic {
    my $state  = shift;
    my $serial = shift;

    if ( rpi() &&  $cfg->{gpio}{dtr} ) {
        gpio( $cfg->{gpio}{dtr}, $state );
    }
    elsif( $zdo ) {
        $zdo->{port}->dtr_active( $state );
    }

    enqueue( ['mic/status' => $state] );
}

sub shutdown {
    kill 'KILL', @threads;
    trace('Graceful shutdown');
    die "Good bye\n";
}

END {
}

1;
