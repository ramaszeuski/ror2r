package ROR2N::Tools::Logger;
use strict;
use warnings;

use Log::Dispatch;
use Log::Dispatch::Syslog;
use Log::Dispatch::Screen;
use Time::HiRes qw( gettimeofday usleep );
use Date::Manip;

use constant SYSLOG_IDENT        => 'ror2n';

use constant DEFAULT_LEVEL       => 'info';
use constant DEFAULT_MAX_LEVEL   => 'emergency';

use constant DEFAULT_CFG => {
    syslog => 'error',
    screen => 'error',
};

sub new {
    my $classname = shift;
    my $args      = shift;

    $args->{screen}       //= DEFAULT_CFG->{screen};
    $args->{syslog}       //= DEFAULT_CFG->{syslog};
    $args->{syslog_ident} //= SYSLOG_IDENT;

    my $self = {};
    bless ( $self, $classname );

    $self->{logger} = Log::Dispatch->new();

    if ( $args->{screen} =~ /(\w+)(,(\w+))?/ ) {

        my ( $min_level, $max_level ) = ( $1, $3 // DEFAULT_MAX_LEVEL );

        if (
            $self->{logger}->level_is_valid($min_level)
            &&
            $self->{logger}->level_is_valid($max_level)
        ) {
            $self->{logger}->add(
                Log::Dispatch::Screen->new(
                    name      => 'screen',
                    min_level => $min_level,
                    max_level => $max_level,
                    newline   => 1,
                    stderr    => 0,
                )
            );
        }
    }

    if ( $args->{syslog} =~ /(\w+)(,(\w+))?/ ) {

        my ( $min_level, $max_level ) = ( $1, $3 // DEFAULT_MAX_LEVEL );

        if (
            $self->{logger}->level_is_valid($min_level)
            &&
            $self->{logger}->level_is_valid($max_level)
        ) {
            $self->{logger}->add(
                Log::Dispatch::Syslog->new(
                    name      => 'syslog',
                    min_level => $min_level,
                    max_level => $max_level,
                    ident     => $args->{syslog_ident},
                )
            );
        }
    }

	return $self;
}

sub trace {
    my ( $self, $message, $level, $args ) = @_;

    # na zacatku. kvuli minimailzaci odchylky
    my ($s, $us) = gettimeofday();

    if ( ! ( $level && $self->{logger}->level_is_valid($level)) ) {
        $level = DEFAULT_LEVEL;
    }

    return if ! exists $self->{logger};
    return if ! $self->{logger}->would_log( $level );

#    if ( ref $message eq 'HASH') {
#        $message = Dump $message;
#    }

#    if ( $args->{hexdump} ) {
#        $message = "\n" . HexDump( $message );
#    }

    my @outputs;
    if ( $args->{output} ) {
        @outputs = ( ref $args->{output} eq 'ARRAY' )
                 ? @{ $args->{output} }
                 : ( $args->{output} )
                 ;

    }
    else {
        @outputs = keys %{ $self->{logger}{outputs} };
    }


    OUTPUT:
    foreach my $output ( @outputs ) {
        next OUTPUT if ! exists $self->{logger}{outputs}{$output};

        my $prefix = "$0";

        if ( $output =~ /^(file|screen)/) {
            $prefix = sprintf ('%s.%06d %s',
                UnixDate( ParseDate("epoch $s"), '%Y-%m-%d %H:%M:%S' ),
                $us,
                $prefix
            );
        }

        my @msg = ();

        if ( ref $message eq 'ARRAY') {
            $message = join "\n", @{ $message };
        }

       if ( $output eq 'syslog' ) {
            $message =~ s/\r/^M/g;
            @msg = split "\n", $message;
        }
        else {
            @msg = ( $message );
        }

        MSG:
        foreach my $msg ( @msg ) {
            $self->{logger}->log_to(
                name    => $output,
                level   => $level,
                message => "$prefix> $msg",
            )
        }
    }
}

1;

__END__

