package ROR2N::Tools::Commands;

use strict;
use warnings;

use Module::Pluggable search_path => [
    __PACKAGE__ ,
], require => 1;

sub new {
    my $classname = shift;
    my $args      = shift; #logger

    my $self = {};
    bless ( $self, $classname );

    $self->plugins();

    $self->{logger} = $args->{logger};

	return $self;
}

sub process_request {
    my $self   = shift;
    my $msg_in = shift;

    $msg_in =~ s/\W*$//;

    $self->trace( "Command $msg_in" );

    my ($cmd, @args) = split /\W+/, lc( $msg_in );

    my $plugin = __PACKAGE__ . "::$cmd";

    if (! $plugin->can('request')) {
        $self->trace("Unknown command: '$cmd'", 'warn');
        return;
    }

    $plugin->request( $self, @args );

}

sub trace {
    my $self = shift;
    $self->{logger}->trace( @_ );
}

1;

__END__
