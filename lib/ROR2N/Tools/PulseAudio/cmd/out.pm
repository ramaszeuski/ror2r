package ROR2N::Tools::PulseAudio::cmd::out;

use strict;
use warnings;
use Time::HiRes qw( usleep );

use constant FADE_STEP   => 250000; #0.5s
use constant ONE_PERCENT => 65536/100;

sub request {
    my $self  = shift;
    my $pa    = shift;

    my $cmd   = shift;

    my $id   = $pa->{inventory}{default}{sink};
    my $name = $pa->{inventory}{sinks}{$id}{id};

    if ( $cmd =~ /on/i ) {
        $pa->request( "suspend-sink $id off" );
        $pa->request( "set-sink-mute $id off" );
        return undef;
    }
    elsif ( $cmd =~ /off/i ) {
        $pa->request( "set-sink-mute $id on" );
        return undef;
    }
    elsif ( $cmd =~ /volume/i ) {
        my $volume = $_[0];
        if ( defined $volume ) {
            $pa->request( "set-sink-volume $id " . int($volume * ONE_PERCENT) );
        }
        else {
            my $rc = ROR2N::Tools::PulseAudio::parser::dump->parse(
                $pa->request( 'dump' )
            );

            $volume = $rc->{sink}{ $name }{volume};
        }
        main::enqueue(['sound/volume/out', $volume]);
    }
    elsif ( $cmd =~ /fade/i ) {
        my ( $new_volume, $time ) = @_;

        $new_volume //= 0;
        $time       //= 3;

        my $rc = ROR2N::Tools::PulseAudio::parser::dump->parse(
            $pa, $pa->request( 'dump' )
        );

        my $current_volume = $rc->{sink}{ $name }{volume};

        my $step_count = int($time * 1000000 / FADE_STEP);
        my $step = int (( $new_volume - $current_volume ) / $step_count);

        STEP:
        while ( $step_count-- ) {
            $current_volume += $step;
            $pa->request( "set-sink-volume $id " . int($current_volume * ONE_PERCENT));
            main::enqueue(['sound/volume/out', $current_volume]);
            usleep(FADE_STEP);
        }
    }
}

1;

