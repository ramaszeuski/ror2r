package ROR2N::Tools::PulseAudio::cmd::mic;

use strict;
use warnings;

use constant ONE_PERCENT => 65536/100;

sub request {
    my $self  = shift;
    my $pa    = shift;

    my $cmd   = shift;

    my $id   = $pa->{inventory}{default}{source};
    my $name = $pa->{inventory}{sources}{$id}{id};

    if ( $cmd =~ /on/i ) {
        $pa->request( "suspend-source $id off" );
        $pa->request( "set-source-mute $id off" );
        return undef;
    }
    elsif ( $cmd =~ /off/i ) {
        $pa->request( "set-source-mute $id on" );
        return undef;
    }
    elsif ( $cmd =~ /volume/i ) {
        my $volume = $_[0];

        if ( defined $volume ) {
            $pa->request( "set-source-volume $id " . int($volume * ONE_PERCENT) );
        }
        else {
            my $rc = ROR2N::Tools::PulseAudio::parser::dump->parse(
                $pa->request( 'dump' )
            );

            $volume = $rc->{source}{ $name }{volume};
        }
        main::enqueue(['sound/volume/mic', $volume]);
    }
}

1;
