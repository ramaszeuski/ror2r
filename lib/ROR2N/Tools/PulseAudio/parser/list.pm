package ROR2N::Tools::PulseAudio::parser::list;

use strict;
use warnings;

sub parse {
    my $self  = shift;

    my $rc = {};

    my $id;

    LINE:
    foreach my $line ( @_ ) {
        chomp $line;
        if ( $line =~ /^\s+(\*)*\s+index:\s+(\d+)/ ) {
            $id = $2;
            $rc->{ $id }{default} = $1 ? 1 : 0;
            $rc->{ $id }{number}  = $id;
        }
        elsif ( $line=~ /^\s+state+:\s+(\w+)/ ) {
            $rc->{ $id }{state} = $1;
        }
        elsif ( $line=~ /^\s+monitor\ssource:\s+(\d+)/ ) {
            $rc->{ $id }{monitor_source} = $1;
        }
        elsif ( $line=~ /^\s+monitor_of:\s+(\d+)/ ) {
            $rc->{ $id }{monitor_of} = $1;
        }
        elsif ( $line=~ /^\s+name:\s+<([^>]+)>/ ) {
            $rc->{ $id }{id} = $1;
        }
        elsif ( $line=~ /^\s+alsa\.card_name\s+=\s+"([^"]+)"/ ) {
            $rc->{ $id }{card_name} = $1;
        }
    }

    return $rc;

}

1;
