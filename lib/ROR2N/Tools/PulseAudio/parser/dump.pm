package ROR2N::Tools::PulseAudio::parser::dump;

use strict;
use warnings;
use constant ONE_PERCENT => 65536/100;

sub parse {
    my $self  = shift;

    my $rc = {};

    LINE:
    foreach my $line ( @_ ) {
        chomp $line;

        if ( $line =~ /^load-module\s(\S+)(\s(.+))*/ ) {
            $rc->{module}{$1} = $3;
        }
        elsif ( $line =~ /^set-(sink|source)-mute\s(\S+)\s(\w+)/ ) {
            $rc->{$1}{$2}{mute} = ( $3 eq 'yes' ) ? 1 : 0;
        }
        elsif ( $line =~ /^set-(sink|source)-volume\s(\S+)\s([\dabcdefx]+)/i ) {
            $rc->{$1}{$2}{volume} = int(hex($3)/ONE_PERCENT);
        }
        elsif ( $line =~ /^suspend-(sink|source)\s(\S+)\s(\w+)/ ) {
            $rc->{$1}{$2}{suspend} = ( $3 eq 'yes' ) ? 1 : 0;
        }
        elsif ( $line =~ /^set-default-(sink|source)\s(\S+)/ ) {
            $rc->{$1}{$2}{default} = 1;
        }
    }

    return $rc;

}

1;
