package ROR2N::Tools::PulseAudio;
use strict;
use warnings;

use constant DEFAULT_HOST   => '127.0.0.1';
use constant DEFAULT_PORT   => 4712;

use constant RESPONSE_TIMEOUT => 0.1;

use constant UNSUPPORTED_DEVICES => {
    'bcm2835 ALSA' => 1,
};


use Module::Pluggable search_path => [
    __PACKAGE__ . '::cmd',
    __PACKAGE__ . '::parser',
], require => 1;

use IO::Socket;
use Time::HiRes qw( alarm );

sub new {
    my $classname = shift;
    my $args      = shift; # websocket

    my $self = {};
    bless ( $self, $classname );

    if ( $args->{socket} ) {
        $self->{socket} = $args->{socket};
    }
    else {
        $self->{host} = $args->{host} // DEFAULT_HOST;
        $self->{port} = $args->{port} // DEFAULT_PORT;
    }

    $self->plugins();
    $self->inventory();

    return $self;
}

sub process_request {
    my $self   = shift;
    my $msg_in = shift;

    $msg_in =~ s/\W*$//;

    $self->trace( "Command for pulseaudio $msg_in" );

    my ($cmd, @args) = split /\W+/, lc( $msg_in );

    my $plugin = __PACKAGE__ . "::cmd::$cmd";

    if (! $plugin->can('request')) {
        $self->trace("Unknown command for pulseaudio: '$cmd'", 'warn');
        return;
    }

    $plugin->request( $self, @args );

}

sub request {
    my $self = shift;
    my $cmd  = shift;

    my $socket;

    if ( $self->{socket} ) {
        $socket = IO::Socket::UNIX->new(
            Type => SOCK_STREAM(),
            Peer => $self->{socket},
        );
    }
    else {
        $socket = IO::Socket::INET->new(
            PeerAddr => $self->{host},
            PeerPort => $self->{port},
            Proto    => "tcp",
            Type     => SOCK_STREAM,
            Timeout  => 1,
        );
    }


    if ( ! $socket ) {
        $self->trace( "Couldn't connect to pulseaudio", 'error' );
        return;
    }

    $self->trace( "Outgoing message to pulseaudio '$cmd'", 'debug' );

    print $socket "$cmd\n";

    local $SIG{ALRM} = sub { die 'timeout' };
    alarm(RESPONSE_TIMEOUT);

    my @response = ();

    eval {
        while ( my $line = readline($socket) ) {
            push @response, $line;
        }
    };

    alarm(0);

    $socket->close();

    return @response;
}

sub trace {
    my $self  = shift;
    if (main->can('trace')) {
        main::trace( @_ );
    }
}

sub inventory {
    my $self = shift;

    my $default = {};

    my $parser = __PACKAGE__ . "::parser::list";

    my $sinks = $parser->parse(
        $self->request('list-sinks')
    );

    # najit defaultni sink
    SINK:
    foreach my $id ( keys %{ $sinks } ) {
        if ( ! $sinks->{$id}{card_name}
             || UNSUPPORTED_DEVICES->{ $sinks->{$id}{card_name}}
        ) {
            delete $sinks->{ $id };
            next SINK;
        }

        # fallback
        $default->{sink}    //= $id;
        $default->{monitor} //= $sinks->{$id}{monitor_source};

        if ( $sinks->{$id}{default} ) {
            $default->{sink}    = $id;
            $default->{monitor} = $sinks->{$id}{monitor_source};
        }

    }
    $self->request("set-default-sink " . $default->{sink});


    my $sources = $parser->parse(
        $self->request('list-sources')
    );

    # njit defaultni vstup, vyhodit monitory
    SOURCE:
    foreach my $id ( keys %{$sources} ) {
        if ( ! $sources->{$id}{card_name}
             || UNSUPPORTED_DEVICES->{ $sources->{$id}{card_name}}
        ) {
            delete $sources->{ $id };
            next SOURCE;
        }

        if ( exists $sources->{$id}{monitor_of} ) {
            delete $sources->{$id};
            next SOURCE;
        }

        $default->{source} //= $id;

        if ( $sources->{$id}{default} ) {
            $default->{source} = $id;
        }
    }
    $self->request("set-default-source " . $default->{source});

    $self->{inventory} = {
        sinks    => $sinks,
        sources  => $sources,
        default  => $default,
    };

    return $self->{inventory};

}

1;

__END__

