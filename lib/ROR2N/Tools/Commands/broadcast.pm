package ROR2N::Tools::Commands::broadcast;

use strict;
use warnings;

use Audio::MPD;
use POSIX qw(strftime);
use JSON;

sub request {
    my $self  = shift;
    my $cmd   = shift;

    my $id    = shift;

    my $mpd = Audio::MPD->new();

    my $relation;

    if ( $id eq 'end' ) {
        $cmd->process_request('mic off');

        # ukonceni vysilani
        if ( main::shm('broadcast_relation') ne 'none') {
            $relation = $main::db->resultset('Relation')->find(
                { id => main::shm('broadcast_relation') },
            );
        }

        # prehrat unkoncovaci znelky
        if ( $relation ) {
            my @ending = $relation->ending();
            if ( scalar @ending ) {
                $mpd->playlist->clear;
                eval { $mpd->playlist->add( map { $_ . '.mp3' } @ending ); };
                $mpd->play();
                main::enqueue( ['broadcast/status', 'ending'] );
                return;
            }
        }

        $cmd->process_request('off');
        return;
    }

    $relation = $main::db->resultset('Relation')->find(
        { id => $id },
    );

    if ( ! $relation ) {
        main::error( 'relation_404' );
        return;
    }

    if ( ! $relation->channels ) {
        main::error('no_channels');
        return;
    }

    # ZAPNUTI
    my $target = $relation->channels;
    if ( $target =~ /general/ ) {
        $target = 'all'
    }
    else {
        $target = "channels $target";
    }

    if ( $relation->testing ) {
        $target .= ' test';
    }

    if ( $main::cfg->{hardware}{type} eq 'zdo' && $main::zdo ) {
        $main::zdo->on($target);
    }
    elsif ( $main::cfg->{hardware}{type} eq '100v' ) {
        $main::rpi->on();
        main::enqueue( ['broadcast/status', 'allowed'] );
    }
    else {
        main::enqueue( ['broadcast/status', 'allowed'] );
    }

    my $status = main::shm('broadcast_status');

    if ( (! $status ) || ($status !~ /allowed|continued/) ) {
        $self->error_report( $relation, 500);
        return;
    }

    $main::pa->process_request('out on');

    # nastaveni ulozene hlasitosti
    main::trace( 'Loading volume' );
    my $volume = $main::db->resultset('Config')->find({ id => 'volume' });
    if ( $volume ) {
        $volume = from_json( $volume->value());
        if ( $volume->{out}{volume} ) {
            $main::pa->process_request('out volume ' . $volume->{out}{volume});
        }
    }

#   vzdy vypnout mikrofon
    if ( $relation->id > 0 ) {
        $cmd->process_request('mic off');
    }

    main::enqueue( ['broadcast/relation', $relation->id] );
    main::enqueue( 'publish', ['mpd/cmd', 'play 0 ' . $relation->id] );

}

sub error_report {
    my $self     = shift;
    my $relation = shift;
    my $status   = shift;

    # u ziveho vysilani udelat kopii a nastavit datim
    if ( ! $relation->id ) {
        my $properties = $relation->properties;

        $properties->{live} = 1;

        $relation = $relation->copy(
            { properties => $properties }
        );
        $relation->update(
            {
                scheduled   => strftime("%Y-%m-%d %H:%M:%S", localtime),
            }
        );
    }

    my $report = $relation->make_report(
        {
            status_id => $status,
        }
    );

    main::enqueue( 'publish', [ 'broadcast/report' => $relation->id ] );
    main::enqueue( 'publish', [ 'alert' => 'relation' ] );
}

1;

__END__

