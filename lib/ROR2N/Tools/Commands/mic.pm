package ROR2N::Tools::Commands::mic;

use strict;
use warnings;

use Audio::MPD;

sub request {
    my $self = shift;
    my $cmd  = shift;
    my $req  = shift;

    if ( $req eq 'on' ) {
    # v pripade ze neco hraje - ztlumit zvuk
        my $mpd = Audio::MPD->new();
        my $status = $mpd->status();

        if ( $status && $status->state eq 'play' ) {
            $main::pa->process_request('out volume');
            my $volume = main::shm('sound_volume_out');
            $main::pa->process_request('out fade 0 2');
            $mpd->stop;
            $main::pa->process_request("out volume $volume");
        }

        main::mic(1);
        $main::pa->process_request('mic on');
    }
    else {
        main::mic(0);
        $main::pa->process_request('mic off');
    }

}


1;
