package ROR2N::Tools::Commands::off;

use strict;
use warnings;
use Time::HiRes qw( usleep );

sub request {
    my $self  = shift;
    my $cmd   = shift;

    if ( $main::cfg->{hardware}{type} eq 'zdo' && $main::zdo ) {
        $main::zdo->off();
    }
    elsif ( $main::cfg->{hardware}{type} eq '100v' ) {
        my $gpio = $main::cfg->{gpio}{'100v'};
        if ( $gpio && main::rpi() ) {
            $main::rpi->gpio( $gpio, 0 );
        }
        main::enqueue( ['broadcast/status', 'denied'] );
    }

    $cmd->process_request('mic off');

    my $mpd = Audio::MPD->new();
    $mpd->stop();
    $mpd->repeat(0);
    $mpd->playlist->clear();

    main::enqueue( ['broadcast/relation', 'none'] );
}

1;
