package ROR2N::Tools::Commands::snd;

use strict;
use warnings;

use JSON;

sub request {
    my $self   = shift;
    my ($cmd, $target, $param, $value) = @_;

    my $db = $main::db->resultset('Config');

    return if $target !~ /^mic|out$/;
    return if $param !~ /^volume$/;
    return if $value !~ /^\d+$/;

    my $cfg = $db->find(
        {
            id => 'volume',
        }
    );

    my $data = $cfg ?  from_json( $cfg->value() ) : {};

    $data->{$target}{$param} = $value;

    $cfg->update({ value => to_json($data)});
}

1;
