package ROR2N::Tools::Commands::reboot;

use strict;
use warnings;

use constant FLAG  => '/var/lib/ror/reboot';
use constant TOUCH => '/usr/bin/touch';

sub request {
    my $self  = shift;
    my $delay = shift;

    if ( $delay =~ /^\d{1,2}$/ ) {
        sleep( $delay );
    }

    system(TOUCH, FLAG);
}

1;
