package ROR2N::Hardware::ZDO::msg::board_to_speaker;

use strict;
use warnings;

use ROR2N::Hardware::ZDO;

sub decode {
    my $self = shift;
    my $zdo  = shift;

    my @response = ();

    if ( $_[1] ) {
#       push @response, join (' ', 'ack on speaker', $_[1]);
        if ( $_[3] & 0x80 ) {
            push @response, ['zdo/status' => 1];
        }
        else {
            push @response, ['zdo/status' => 0];
        }
    }
    elsif ( $_[4] & 0xa0 ) {
        push @response, ['broadcast/status' => 'allowed'];
        $zdo->clear_response_timeout('on speaker');
    }

    return @response;

}

1;

