package ROR2N::Hardware::ZDO::msg::board_to_pc;

use strict;
use warnings;

use ROR2N::Hardware::ZDO;

sub decode {
    my $self = shift;
    my $zdo  = shift;

    my @response = ();

    if ( $_[6] == 0x15 ) {
        push @response, ['r3/status' => 1];
        $zdo->clear_response_timeout('test r3')
    }
    elsif ( $_[6] == 0x16 ) {
        push @response, ['zdo/voice', 1];
        $zdo->clear_response_timeout('voice')
    }
    elsif ( $_[6] == 0x17 ) {
        push @response, ['zdo/voice', 0];
        $zdo->clear_response_timeout('voice')
    }

    return @response;

}

1;
