package ROR2N::Hardware::ZDO::msg::board2_to_pc;

use strict;
use warnings;

use Date::Manip;
use JSON;
use ROR2N::Hardware::ZDO;

sub decode {
    my $self = shift;
    my $zdo  = shift;

    my @response = ();

    if ( $_[0] == 0xc ) {
        $zdo->clear_response_timeout('settime')
    }
    elsif ( $_[0] == 0x12 ) {
        push @response, ['zdo/eeprom' => 'missed'];
        $zdo->clear_response_timeout('eeprom')
    }
    elsif ( $_[0] == 0xd ) {
        push @response, [sprintf('speaker/%d/status', $_[1]), 3 ];
    }
    elsif ( $_[0] == 0xe ) {
        push @response, [sprintf('speaker/%d/status', $_[1]), 2 ];
    }
    elsif ( $_[0] == 0x3 ) {
        push @response, [sprintf('speaker/%d/status', $_[1]), 0 ];
        $zdo->clear_response_timeout('test speaker')
    }
    elsif ( $_[0] == 0x2 ) {
        my $timestamp = Date_SecsSince1970(
            $_[6],
            $_[5] + 1,
            $_[7] + 2000,
            $_[2],
            $_[3],
            $_[4],
        );

        push @response, [sprintf('speaker/%d/status', $_[1]), 1 ];
        push @response, [sprintf('speaker/%d/time', $_[1]), $timestamp ];
        $zdo->clear_response_timeout('test speaker')
        #TODO:  bajty
    }
    elsif ( $_[0] == 0x18 ) {

        my $info = {
            event => 2,
            a1 => $_[2] + 256 * $_[3],
            a2 => $_[4] + 256 * $_[5],
            a3 => $_[6] + 256 * $_[7],
        };

        push @response, [ sprintf('speaker/%d/info', $_[1]), to_json( $info ) ];


    }
    elsif ( $_[0] == 0x1f ) {
##        my $info = {
##        };

        push @response, [sprintf('speaker/%d/alarm', $_[1]), 1 ];
    }

    return @response;

}



1;

