package ROR2N::Hardware::ZDO::msg::board_to_all;

use strict;
use warnings;

use Audio::MPD;

use constant NO_ACTIVITY => 'none';

use constant ACTIVITY => {
    0x01 => 'pc',
    0x02 => 'console',
    0x04 => 'audio',
    0x08 => 'phone',
    0x10 => 'test',
};

sub decode {
    my $self = shift;
    my $zdo  = shift;

    my %activity = %{ ACTIVITY() };

    my @response = ();

    my $mpd = Audio::MPD->new();

    # stav desky
    push @response, ['zdo/status', ( $_[3] & 0x80 ) ? 1 : 0];

    # vypnuti
    if ( $_[2] & 0x01 ) {
        $mpd->stop;
        $mpd->playlist->clear;

        main::enqueue('publish', ['zdo/off', 1]); # aby nebylo retain
        push @response, ['broadcast/status', 'denied'];
        push @response, ['zdo/activity', 'none'];
        $zdo->clear_response_timeout('off');
    }

    # alarmy
# TODO: zjistit je to pro cteni nebo pro zapis?
#    if ( $_[2] & 0x20 ) {
#        push @response, ['zdo/alarms' => 'allowed'];
#    }

    if ( $_[2] & 0x40 ) {
        push @response, ['zdo/alarm'  => 'manual'];
        $mpd->stop;
        $mpd->playlist->clear;
    }

    if ( $_[2] & 0x80 ) {
        push @response, ['zdo/alarm' => 'jsvv'];
        $mpd->stop;
        $mpd->playlist->clear;
    }

    # zjisteni aktivit
    my $activity = NO_ACTIVITY;

    ACTIVITY:
    foreach my $mask ( keys %activity ) {
        if ( $_[3] & $mask ) {
            $activity =  $activity{$mask};
            if ( $activity eq 'pc') {
                $zdo->clear_response_timeout('on');
            }
            last ACTIVITY;
        }
    }
    push @response, ['zdo/activity', $activity ];

    #broadcast
    if ( $_[4] & 0x80 ) {
        push @response, ['broadcast/status',  'allowed'];
    }

    $zdo->clear_response_timeout('test board');

    return @response;

}

1;
