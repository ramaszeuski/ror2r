package ROR2N::Hardware::ZDO::cmd::test;

use strict;
use warnings;

use constant TIMEOUT              => 1;
use constant TIMEOUT_TEST_SPEAKER => 25;

sub make {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my $src     = $zdo->addr('pc');
    my $dst     = $zdo->addr('board');
    my @data    = $zdo->data();

    my $target = shift(@args) // 'null';

    # Test zakladni desky
    if ( $target eq 'board' ) {
        main::enqueue( 'retain', ['zdo/status', -1 ] );
        $data[3] = 0x40;
    }

    # Test pripojeni rozvadece R3
    elsif ( $target eq 'r3') {
        $data[6] = 0x15;
    }

    # Test obousmerneho hlasice
    elsif ( $target eq 'speaker' ) {
        my $id = $args[0];

        if ( ! $zdo->valid_speaker_id($id) ) {
            $zdo->trace("Invalid speaker id: $id", 'warn');
            return;
        }

        $data[0] = 0x1;
        $data[1] = $id;
        $dst     = $zdo->addr('board2');

        $target  = $zdo->speaker_key( $id ); # id pro shm

        return ( $src, $dst, TIMEOUT_TEST_SPEAKER, @data );
    }

    else {
        $zdo->trace("Unknown check target: $target", 'warn');
        return;
    }

    return ( $src, $dst, TIMEOUT, @data );

}

sub timeout {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my @response = ();

    my $target = shift(@args) // 'null';

    if ( $target eq 'board' ) {
        main::enqueue('retain', ['zdo/status', 0]);
    }
    elsif ( $target eq 'speaker' ) {
        main::enqueue('publish', [sprintf('speaker/%d/status', $args[0]), 0]);
    }
}

sub nowait {
    return 1;
}

1;
