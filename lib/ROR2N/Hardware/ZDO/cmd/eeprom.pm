package ROR2N::Hardware::ZDO::cmd::eeprom;

use strict;
use warnings;

use constant TIMEOUT => 1;

sub make {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my $src     = $zdo->addr('pc');
    my $dst     = $zdo->addr('board2');
    my @data    = $zdo->data();

    $data[0] = 0x6;

    return ( $src, $dst, TIMEOUT, @data);
}

1;

