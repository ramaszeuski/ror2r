package ROR2N::Hardware::ZDO::cmd::on;

use strict;
use warnings;

use constant TIMEOUT => 10;

sub make {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my $src     = $zdo->addr('pc');
    my $dst     = $zdo->addr('board');
    my @data    = $zdo->data();

    my $target = shift(@args) // 'all';

    if ( scalar @args && $args[-1] eq 'test' ) {
        $data[3] = 0x20;
        pop @args;
    }

    # Vsechny okruhy
    if ( $target eq 'all' ) {
        $data[2] = 0x2;
    }

    # Jednotlive okruhy
    elsif ( $target eq 'channels') {
        my $channels = 0;

        CHANNEL:
        foreach my $channel ( @args ) {
            next CHANNEL if ! $zdo->valid_channel_id($channel);
            my $mask = 1 << ( $channel - 1 );
            $channels |= $mask;
        }
        ($data[0], $data[1]) = unpack('CC', pack('S', $channels));
    }

    # Jeden hlasic
    elsif ( $target eq 'speaker' ) {

        my $id = $args[0];

        if ( ! $zdo->valid_speaker_id($id) ) {
            $zdo->trace("Invalid speaker id: $id", 'warn');
            return;
        }

        $data[1] = $id;
        $data[4] = 0x20;
        $src     = $zdo->addr('speaker');
    }

    else {
        $zdo->trace("Unknown target: $target", 'warn');
        return;
    }

    return ( $src, $dst, TIMEOUT, @data);
}

1;
