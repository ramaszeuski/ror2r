package ROR2N::Hardware::ZDO::cmd::settime;

use strict;
use warnings;

use constant TIMEOUT => 1;

sub make {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my $src     = $zdo->addr('pc');
    my $dst     = $zdo->addr('board2');
    my @data    = $zdo->data();

    $data[0] = 0x7; #constant
    (
        $data[4],
        $data[3],
        $data[2],
        $data[5],
        $data[6],
        $data[7],

    ) = localtime(time);
    $data[7] = sprintf("%02d", $data[7] % 100);

    return ( $src, $dst, TIMEOUT, @data);
}

1;


