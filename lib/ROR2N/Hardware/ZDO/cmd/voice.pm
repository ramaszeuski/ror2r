package ROR2N::Hardware::ZDO::cmd::voice;

use strict;
use warnings;

use constant TIMEOUT => 1;

sub make {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my $src     = $zdo->addr('pc');
    my $dst     = $zdo->addr('board');
    my $timeout = $zdo->timeout('default');
    my @data    = $zdo->data();

    my $state = shift(@args);

    $state = 1 if $state =~ /on/;
    $state = 0 if $state =~ /off/;
    $data[6] = $state ? 0x16 : 0x17;

    return ( $src, $dst, TIMEOUT, @data);
}

1;

