package ROR2N::Hardware::ZDO::cmd::off;

use strict;
use warnings;

use constant TIMEOUT => 1;

sub make {
    my $self = shift;
    my $zdo  = shift;
    my @args = @_;

    my $src     = $zdo->addr('pc');
    my $dst     = $zdo->addr('board');
    my @data    = $zdo->data();

    $data[2] = 0x1;
    $data[2] = 0x1;

    return ( $src, $dst, TIMEOUT, @data);
}

1;
