package ROR2N::Hardware::RPi;

use strict;
use warnings;

sub new {
    my $classname = shift;
    my $args      = shift; # logger device

    my $self = {};
    bless ( $self, $classname );

    open (CPUINFO, '/proc/cpuinfo');

    LINE:
    while (<CPUINFO>) {
        if (/Hardware.+BCM2/i) {
            $self->{compatible} = 1;
            last LINE;
        }

        next LINE if ! $self->{compatible};

        if (/^Revision\s+:\s+(.+)$/i) {
            $self->{revision} = $1;
            last LINE;
        }

        # http://www.raspberrypi-spy.co.uk/2012/09/checking-your-raspberry-pi-board-version/
        if ( $self->{model} && $self->{revision} ) {

            $self->{version} = 0;

            if ( $self->{revision} =~ /^000[def]$/ ) {
                $self->{version} = 1;
            }
            elsif ( $self->{revision} =~ /^001[03]$/) {
                $self->{version} = 1;
            }
            elsif ( $self->{revision} =~ /^a[02]1041$/) {
                $self->{version} = 2;
            }
            elsif ( $self->{revision} =~ /^a[02]2082$/) {
                $self->{version} = 3;
            }

            last LINE;
        }
    }

    return $self;
}

sub gpio {
    my $self = shift;

    if ( ! $self->{compatible} ) {
        return undef;
    }

    my $id = shift;

    my $value;

    # WRITE
    if ( scalar @_ ) {
        $value = shift;
        my $val = $value ? 'dh':'dl';
	    `raspi-gpio set $id $val`;
    }
    else {
        $value = 'raspi-gpio get $id';
	    if ($value =~ /level=(\d)/ ) {
            $value = $1;
        }
    }

    return $value;

}

sub on {
    my $self = shift;

    my $gpio = $main::cfg->{gpio}{'100v'};

    if ( $gpio ) {
        $self->gpio( $gpio, 1 );
    }
    if ( $main::cfg->{'100v'} && $main::cfg->{'100v'}{warm_up_time} ) {
        sleep($main::cfg->{'100v'}{warm_up_time});
    }
}

1;
__END__
