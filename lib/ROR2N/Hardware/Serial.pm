package ROR2N::Hardware::Serial;

use strict;
use warnings;

use constant DEVICE_BASE => '/dev/ttyUSB';
use constant BAUDRATE    => 19200;
use constant PARITY      => 'none';
use constant DATABITS    => 8;
use constant STOPBITS    => 1;
use constant PARITY      => 'none';
use constant HANDSHAKE   => 'none';

use constant SERIAL_CHECK_SLEEP => 50000; # us
use constant SERIAL_ERROR_SLEEP => 10;    # s

use constant SLIP_END     => 0xC0;
use constant SLIP_ESC     => 0xDB;
use constant SLIP_ESC_END => 0xDC;
use constant SLIP_ESC_ESC => 0xDE;

use constant STX          => 0x02;
use constant ETX          => 0x03;

use Device::SerialPort;
use Time::HiRes qw( gettimeofday tv_interval usleep);
use YAML;

sub new {
    my $classname = shift;
    my $args      = shift; # logger device

    my $self = {};
    bless ( $self, $classname );

    $self->connect( $args );

	return $self;
}

sub connect {
    my $self = shift;
    my $args = shift;

    $args->{device} ||= DEVICE_BASE;

    my @devices = ( $args->{device} =~ /\d$/)
                ? ( $args->{device} )
                : map { $args->{device} . $_ } ( 0 .. 9 )
                ;

    my $port;

    DEV:
    foreach my $device ( @devices ) {
        next DEV if ! -c $device;
        next DEV if ! -w $device;

        $self->trace ("Try to use serial device $device", 'debug');
        $port = new Device::SerialPort ( $device );

        if ( $port ) {
            $self->{device} = $device;
            $self->trace("Successfull connected serial device $device");
            last DEV;
        }
        else {
            $self->trace ("Can't open $device: $^E", 'warn');
        }
    }

    if ( ! $port ) {
        $self->trace ("Can't open serial port", 'critical');
        return;
    }

    $port->baudrate(  $args->{baudrate}  || BAUDRATE );
    $port->parity(    $args->{parity}    || PARITY );
    $port->databits(  $args->{databits}  || DATABITS );
    $port->stopbits(  $args->{stopbits}  || STOPBITS );
    $port->handshake( $args->{handshake} || HANDSHAKE );

    $port->write_settings;

    $self->{port} = $port;

}

sub read {
    my $self = shift;

    my $char = '';

    STATUS:
    while ( ! length($char) ) {

        my (undef, $in, undef, undef) = $self->{port}->status();

        if ( ! defined $in ) {
            $self->trace( sprintf("Serial port %s status error", $self->{device}), 'critical' );
            $self->{port}->close();
            sleep (SERIAL_ERROR_SLEEP);
            last STATUS;
        }

        if ( ! $in ) {
            usleep( SERIAL_CHECK_SLEEP );
            next STATUS;
        }

        ( my $count, $char ) = $self->{port}->read( 1 );

#print ">>> ";
#print unpack('H*', $char) . "\n";

        if ( ! $count ) {
            $self->trace("Serial read error", 'warn');
            $char = '';
            last STATUS;
        }

#        vypada ze to hodne zpomaluje
#        my $now = gettimeofday();
#        main::shm('READ ' . $self->{device}, $now);

    }

    return length($char) ? unpack ('C*', $char) : undef;
}

sub read_message_slip {
    my $self = shift;

    my @msg = ();

    CHAR:
    while (1) {
        my $char = $self->read();

        if ( $char == SLIP_END ) {
            if ( scalar @msg ) {
                last CHAR;
            }
            else {
                next CHAR;
            }
        }
        elsif ( $char == SLIP_ESC ) {
            $char = $self->read();
            if ( $char == SLIP_ESC_END ) {
                $char = SLIP_END;
            }
            elsif ( $char == SLIP_ESC_ESC ) {
                $char = SLIP_ESC;
            }
        }

        push @msg, $char;

    }

    return @msg;
}

sub pack_message {
    my $self = shift;

    my @msg = ( STX, @_, ETX );
    return (@msg, $self->checksum( @msg ));
}

sub pack_message_slip {
    my $self = shift;

    my @msg = ( STX, @_, ETX );
    @msg = (@msg, $self->checksum( @msg )) ;

    my @slip = ( SLIP_END );

    CHAR:
    foreach my $char ( @msg ) {
        if ( $char == SLIP_END ) {
            push @slip, SLIP_ESC;
            push @slip, SLIP_ESC_END;
        }
        elsif ( $char == SLIP_ESC ) {
            push @slip, SLIP_ESC;
            push @slip, SLIP_ESC_ESC;
        }
        else {
            push @slip, $char
        }
    }

    push @slip, SLIP_END;

    return @slip;
}

sub unpack_message {
    my $self = shift;
    my @msg = @_;

    # overeni kontrolniho souctu
    my $checksum = pop @msg;
    my $expected_checksum = $self->checksum( @msg );

    if ( $checksum != $expected_checksum ) {
        $self->trace(
            'Corrupted message from ' . $self->{device} . ': ' .
            join (' ', map { unpack ('H*', pack('C', $_)) } @msg, $checksum ),
            'warning'
        );
        $self->trace( "Bad checksum - $checksum != $expected_checksum", 'warning' );
        return;
    }

    if ( shift @msg != STX ) {
        $self->trace( 'Missing STX', 'warning' );
        return;
    }

    if ( pop @msg != ETX ) {
        $self->trace( 'Missing ETX', 'warning' );
        return;
    }

    $self->trace( 'Message from ' . $self->{device} . ': ' . join (' ', map { unpack ('H*', pack('C', $_)) } @msg), 'debug');
    return @msg;
}

sub checksum {
    my $self = shift;
    my $checksum = 1 + unpack('C', ~ pack('C', unpack ('%8C*', pack ('C*', @_))));
    $checksum    = 0 if $checksum > 255;

    return $checksum;
}

sub trace {
    my $self  = shift;
    if (main->can('trace')) {
        main::trace( @_ );
    }
}

1;

__END__
