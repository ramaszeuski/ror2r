package ROR2N::Hardware::ZDO;
use base 'ROR2N::Hardware::Serial';

use strict;
use warnings;
use Time::HiRes qw(usleep);

use Module::Pluggable search_path => [
    __PACKAGE__ . '::cmd',
    __PACKAGE__ . '::msg',
], require => 1;

use constant ADDRESS => {
    pc      => 0x80,
    board   => 0x40,
    console => 0x20,
    audio   => 0x10,
    speaker => 0x08,
    all     => 0x04,
    service => 0x02,
    board2  => 0x01,
};

use constant ACTIVITY       => {
    0x01 => 'pc',
    0x02 => 'console',
    0x04 => 'audio',
    0x08 => 'phone',
    0x10 => 'test',
};

use constant DEVICE  => { reverse %{ (ADDRESS()) }};

use constant MIN_CHANNEL          => 1;
use constant MAX_CHANNEL          => 16;
use constant MIN_SPEAKER          => 1;
use constant MAX_SPEAKER          => 255;

use constant BAUDRATE             => 19200;
use constant MESSAGE_SIZE         => 15;
use constant STX                  => 0x02; #TODO: zdedit z Hardware::Serial

use constant RTS_DELAY            => int( MESSAGE_SIZE * 10 * 1000000 / BAUDRATE ); #7812

use constant USLEEP                 => 100000;  #us
use constant USLEEP_BETWEEN_TRY     => 500000; #us
use constant SLEEP_AFTER_OFF        => 12; #s

use constant WAIT_FOR_BROADCAST     => 15; #s

use constant MAX_RESPONSE_TIMEOUT   => 30; #s

use constant CHANNEL_ID_FORMAT      => 'channel_%d';
use constant SPEAKER_ID_FORMAT      => 'speaker_%d';

use constant TRY_REQUEST            => 3;
use constant TRY_TEST               => 3;
use constant TRY_ON                 => 3;
use constant TRY_OFF                => 3;


our %device  = %{ (DEVICE()) };

sub send_request {
    my $self   = shift;
    my $msg_in = shift;

    $msg_in =~ s/\W*$//;

    $self->trace( "Command to ZDO $msg_in" );

    my ($cmd, @args) = split /\W+/, lc( $msg_in );

    my $plugin = __PACKAGE__ . "::cmd::$cmd";

    if ( $plugin->can('nowait') && ($self->last_cmd() || $self->tx()) ) {
        return;
    }

    if (! $plugin->can('make')) {
        $self->trace("Unknown command: '$cmd'", 'warn');
        return;
    }

    my ( $src, $dst, $response_timeout, @data ) = $plugin->make($self, @args);
    return if ! defined $src; #TODO: chyba

    $self->wait_for_unprocessed_request();

    my $msg_out = pack ('C*', ($self->pack_message( $dst, $src, @data )));

    return if ! $msg_out;

    $self->trace( 'Encoded message to ZDO ' . unpack("H*", $msg_out), 'debug' );

    my $rc      = 0;
    my $attempt = 0;

    ATTEMPT:
    while( $attempt++ < TRY_REQUEST ) {

        my $count_out = 0;

        $self->tx(1);
        eval { $count_out = $self->{port}->write( $msg_out ); };
        $self->tx(0);

        if ( $@ ) {
            $self->trace( "Critical error: $@", 'emergency' );
        }
        elsif ( ! $count_out ) {
            $self->trace( "Message send failed", 'error' );
        }
        elsif ( $count_out != length($msg_out ) ) {
            $self->trace( "Message send incomplete", 'error' );
        }
        else {
            $rc = 1;
        }

        next ATTEMPT if ! $rc;

        $self->last_cmd($msg_in);
        $self->trace( "Successfully send $count_out bytes", 'debug' );

    # cekani na odpoved
        eval {
            local $SIG{ALRM} = sub {
                $self->trace('Response timeout', 'warn');
                main::enqueue('publish', ['zdo/timeout', $msg_in]);
                die "timeout\n";
            };

            alarm( $response_timeout );

            WAIT_FOR_RESPONSE:
            while ( $self->last_cmd() ) {
                usleep(USLEEP);
            }

            alarm(0);
        };

        if ( $@ ) {
            $rc = 0;
            if ( $plugin->can('timeout') ) {
                $plugin->timeout($self, @args);
            }
        }

        $self->clear_response_timeout();

        last ATTEMPT if $rc;

        usleep(USLEEP_BETWEEN_TRY);
    }

    if ( $rc ) {
        if ( $attempt > 1 ) {
            $self->trace("Request processed at $attempt attempt", 'warn');
        }
    }
    else {
        $self->trace('Request fail', 'error');
        main::enqueue('retain', ['zdo/status', 0]);
    }

    return $rc;
}

sub read_message {
    my $self = shift;

    while ( $self->tx() ) {
        usleep( RTS_DELAY );
    }

    my @msg = ();

    CHAR:
    while ( ( scalar @msg ) < MESSAGE_SIZE ) {
        my $char = $self->read();
        if ( ( scalar @msg ) || ( $char == STX ) ) {
            push @msg, $char;
        }
    }
    @msg = $self->unpack_message( @msg );

    return if ! scalar @msg;

    my $dst = $device{ shift @msg };
    my $src = $device{ shift @msg };

    if (! $dst || $dst !~ /^(pc|all|speaker)$/ ) {
        $self->trace("This message is not for us: $src > $dst", 'debug');
        return;
    }

    my $plugin = __PACKAGE__ . "::msg::$src" . '_to_' . $dst;

    if (! $plugin->can('decode')) {
        $self->trace("Unknown message type '$src > $dst'", 'warn');
        return;
    }

    $self->trace( "Message from $src to $dst", 'debug' );
    $self->trace( join (' ', 'Message binary dump: ', join ' ', map { unpack ('B8', pack('C', $_)) } @msg), 'debug');

    return $plugin->decode($self, @msg);
}

sub tx {
    my $self  = shift;
    my $state = shift // return main::shm('TX');

    main::shm('TX', $state);

    if ( main::rpi() ) {
        $state = ! $state;
        usleep(RTS_DELAY) if $state;
    }

    $self->{port}->rts_active( $state );

}

sub last_cmd {
    my $self  = shift;
    my $cmd  = shift // return main::shm('LAST CMD');

    main::shm('LAST CMD', $cmd || undef);
}

sub wait_for_unprocessed_request {
    my $self = shift;

    eval {
        local $SIG{ALRM} = sub {
            main::trace('Force remove last_cmd ', 'warn');
            die "timeout\n";
        };

        alarm( MAX_RESPONSE_TIMEOUT );

        WAIT_FOR_RESPONSE:
        while ( $self->last_cmd() ) {
            usleep(USLEEP);
        }

        alarm(0);
    };

    $self->clear_response_timeout();
}


sub clear_response_timeout {
    my $self = shift;
    my $cmd  = shift;

    my $last_cmd = $self->last_cmd();
    return if $cmd && $last_cmd && $last_cmd !~ /^$cmd/i;

    $self->last_cmd('');
}


sub data {
    my $self = shift;
    return (0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
}

sub valid_speaker_id {
    my $self = shift;
    my $id   = shift;
    return 0 if ! defined $id;
    return 0 if $id !~ /^\d+$/;
    return 0 if $id < MIN_SPEAKER;
    return 0 if $id > MAX_SPEAKER;
    return 1;
}

sub valid_channel_id {
    my $self = shift;
    my $id   = shift;
    return 0 if ! defined $id;
    return 0 if $id !~ /^\d+$/;
    return 0 if $id < MIN_CHANNEL;
    return 0 if $id > MAX_CHANNEL;
    return 1;
}

sub addr {
    my $self = shift;
    my $id = shift;

    return ADDRESS->{ $id };
}

sub speaker_key {
    my $self = shift;
    my $id = shift;

    return sprintf(SPEAKER_ID_FORMAT, $id);
}

# makro

sub test {
    my $self = shift;
    my $rc   = 0;

    $self->trace('Testing board');

    TEST:
    foreach ( 1 .. TRY_TEST ) {

        $self->trace("Testing attempt $_");
        $rc = $self->send_request('test board');

        if ( ! $rc ) {
            $self->trace("No answer $_", 'warn');
            usleep(USLEEP_BETWEEN_TRY);
            next TEST;
        }

        my $status = main::shm('zdo_status');
        if  ( defined $status && $status == 1 ) {
            $self->trace('Board OK');
            last TEST;
        }
        else {
            $self->trace('Board error', 'error');
        }

        usleep(USLEEP_BETWEEN_TRY);

    }

    return $rc;
}

sub on {
    my $self = shift;

    my $rc = $self->test();

    return 0 if ! $rc;

    #  V pripade ze probiha vysilani - ukoncit

    my $activity = main::shm('zdo_activity');

    if ( $activity && $activity ne 'none') {

        if ( $activity eq 'pc' ) {
            main::enqueue(['broadcast/status' => 'continued']);
        }
        else {
            main::error("activity $activity");
        }

        return 0;
    }

    # Zapnuti desky a cekani na povleni vysilani

    $rc = 0;

    ON:
    foreach ( 1 .. TRY_ON ) {

        $rc = $self->send_request(join ' ', ('on', @_));

        if ( ! $rc ) {
            usleep(USLEEP_BETWEEN_TRY);
            next ON;
        }

        eval {
            local $SIG{ALRM} = sub { die "timeout\n"; };

            alarm( WAIT_FOR_BROADCAST );

            BROADCAST:
            while (
                ! defined main::shm('broadcast_status')
                ||
                main::shm('broadcast_status') ne 'allowed'
            ) {
                usleep( USLEEP );
            }

            alarm(0);
        };

        if ( $@ ) {
            $self->trace('Broadcast timeout', 'error');
            $self->send_request('off');
            sleep(SLEEP_AFTER_OFF);
        }
        else {
            last ON;
        }
    }

    return $rc;

}

sub off {
    my $self = shift;

    my $rc = $self->test();

    my $activity = main::shm('zdo_activity');

    # neni zapnuto - neni duvod vypinat
    # ale jenom v pripade ze deska vrtaila stav

    if ( $rc && ( ! $activity || $activity eq 'none')) {
        return;
    }

    OFF:
    foreach ( 1 .. TRY_OFF ) {
        if ( $rc = $self->send_request('off') ) {
            last OFF;
        }
        usleep(USLEEP_BETWEEN_TRY);
    }

    return $rc
}

1;

__END__



