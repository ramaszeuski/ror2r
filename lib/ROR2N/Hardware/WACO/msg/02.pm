package ROR2N::Hardware::WACO::msg::02;

# STAVU

use strict;
use warnings;
use JSON;

sub decode {
    my $self = shift;
    my $waco = shift;

    my $speaker  = $_[3];

    my $info = $waco->decode_info(@_);

    return (
        [ sprintf('speaker/%d/status', $speaker), $info->{status} ],
        [ sprintf('speaker/%d/info', $speaker), to_json( $info ) ],
    );
}

1;

