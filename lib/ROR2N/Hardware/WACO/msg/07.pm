package ROR2N::Hardware::WACO::msg::07;

# ODPOVED_INFO

use strict;
use warnings;

sub decode {
    my $self = shift;
    my $waco = shift;

    my @response = ();
    my $speaker  = $_[3];

    push @response, [ sprintf('speaker/%d/volume/2',  $speaker), $_[6]];

    return @response;
}

1;


