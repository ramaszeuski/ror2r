package ROR2N::Hardware::WACO::msg::06;

# ODPOVED_INFO

use strict;
use warnings;

sub decode {
    my $self = shift;
    my $waco = shift;

    my @response = ();
    my $speaker  = $_[3];

    push @response, [ sprintf('speaker/%d/volume/1',  $speaker), $_[5]];

    return @response;
}

1;

