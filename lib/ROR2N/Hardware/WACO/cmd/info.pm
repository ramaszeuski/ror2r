package ROR2N::Hardware::WACO::cmd::info;

use strict;
use warnings;

sub make {
    my $self = shift;
    my $waco = shift;

    my $id    = shift;

    # pred testovanim nastavit stav jako poruchovy
    if ( $main::db ) {
        my $speaker = $main::db->resultset('Speaker')->find( { id => $id} );
        $speaker->update( { status => 0 } ) if $speaker;
    }

    return (0x1, 0, 0x6a, 0, $id, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
}

1;

