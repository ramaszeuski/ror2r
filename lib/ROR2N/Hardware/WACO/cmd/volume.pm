package ROR2N::Hardware::WACO::cmd::volume;

use strict;
use warnings;

use constant MIN_VOLUME  => 0;
use constant MAX_VOLUME  => 254;
use constant MIN_SPEAKER => 1;
use constant MAX_SPEAKER => 255;

sub make {
    my $self = shift;
    my $waco = shift;

    my $id      = shift;
    my $channel = shift;
    my $volume  = shift;

    my ($cmd, @set);

    # validace - prozatim bez chyby
    if ( $id !~ /^\d+$/ || $id < MIN_SPEAKER || $id > MAX_SPEAKER ) {
        $waco->trace("Invalid speaker id: $id", 'warn');
        return;
    }

    if ( $volume !~ /^\d+$/ || $volume < MIN_VOLUME || $volume > MAX_VOLUME ) {
        $waco->trace("Invalid volume: $volume", 'warn');
        return;
    }

    if ( $channel =~ /left|1/i ) {
        $cmd = 0x4;
        @set = ( $volume, 0 );
    }
    elsif ( $channel =~ /right|2/i ) {
        $cmd = 0x5;
#       @set = ( $volume, 0 );
        @set = ( 0, $volume );
    }
    else {
        $waco->trace("Invalid channel: $channel", 'warn');
        return;
    }

    return ($cmd, 0, 0x6a, 0, $id, @set, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 );
}

1;

