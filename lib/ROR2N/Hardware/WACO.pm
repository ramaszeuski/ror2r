package ROR2N::Hardware::WACO;
use base 'ROR2N::Hardware::Serial';

use Module::Pluggable search_path => [
    __PACKAGE__ . '::cmd',
    __PACKAGE__ . '::msg',
], require => 1;

use strict;
use warnings;

sub read_message {
    my $self = shift;

    my @msg = $self->unpack_message(
        $self->read_message_slip()
    );

    return if ! scalar @msg;

    my $type = unpack("H*", pack('C', $msg[0]));

    my $plugin = __PACKAGE__ . "::msg::$type";

    if (! $plugin->can('decode')) {
        $self->trace("Unknown message type '$type'", 'warn');
        return;
    }

#    $self->trace( join (' ', 'Message binary dump: ', join ' ', map { unpack ('B8', pack('C', $_)) } @msg), 'debug');

    return $plugin->decode($self, @msg);
}

sub send_request {
    my $self    = shift;
    my $msg_in  = shift;

    $msg_in =~ s/\W*$//;

    $self->trace( "Command to WACO $msg_in" );

    my ($cmd, @args) = split /\W+/, lc( $msg_in );

    my $plugin = __PACKAGE__ . "::cmd::$cmd";

    if (! $plugin->can('make')) {
        $self->trace("Unknown command: '$cmd'", 'warn');
        return;
    }

    my @msg = $plugin->make($self, @args);
    return if ! scalar @msg;

    $self->trace( 'Message for WACO: ' . join (' ', map { unpack ('H*', pack('C', $_)) } @msg), 'debug');
    @msg = $self->pack_message_slip( @msg );
    $self->trace( 'SLIP Encoded: ' . join (' ', map { unpack ('H*', pack('C', $_)) } @msg), 'debug');

    my $msg_out = pack ('C*', @msg );

    my $count_out;

    eval {
        $self->{port}->rts_active(1);
        $count_out = $self->{port}->write( $msg_out );
        $self->{port}->rts_active(0);
    };

    if ( $@ ) {
        $self->trace( "Critical error: $@", 'emergency' );
    }
    elsif ( ! $count_out ) {
        $self->trace( "Message send failed", 'error' );
    }
    elsif ( $count_out != length($msg_out ) ) {
        $self->trace( "Message send incomplete", 'error' );
    }
    else {
        $self->trace( "Successfully send $count_out bytes to " . $self->{device}, 'debug' );
    }

}

sub decode_info {
    my $self = shift;

    my $info = {
        event       => $_[0],
        opened      => ( $_[8] & 0x01 ) ? 1 : 0,
        amplifier   => ( $_[8] & 0x02 ) ? 1 : 0,
        start       => ( $_[8] & 0x04 ) ? 1 : 0,
        charging    => ( $_[7] & 0x80 ) ? 1 : 0,
        volume1     => $_[5],
        volume2     => $_[6],
        a1          => 256 * $_[10] + $_[11],
        a2          => 256 * $_[12] + $_[13],
        a3          => 256 * $_[14] + $_[15],
    };

    $info->{battery} = 1 if $_[7] & 0x01;
    $info->{battery} = 2 if $_[7] & 0x02;
    $info->{battery} = 3 if $_[7] & 0x04;

    $info->{status} = $self->status($info);

    return $info;
}

sub status {
    my $self = shift;
    my $info = shift;

    my $status = 1;

    $status = 5 if $info->{battery} < 2;
    $status = 5 if $info->{opened};
    $status = 8 if $info->{event} == 0x12;

    return $status;

}


1;

__END__

