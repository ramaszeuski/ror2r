package ROR2N::Schema;

use strict;
use warnings;

use base 'DBIx::Class::Schema';

our $VERSION = 1;

__PACKAGE__->load_namespaces;

sub hms {
    my $self = shift;
    my $sec  = shift;

    my $hour = int($sec / 3600);
    $sec    -= $hour * 3600;

    my $min  = int($sec / 60);
    $sec    -= $min * 60;

    my $hms = sprintf('%02d:%02d:%02d', $hour, $min, $sec);
    $hms =~ s/^00://;
    $hms =~ s/^0//;

    return $hms;
}

1;
