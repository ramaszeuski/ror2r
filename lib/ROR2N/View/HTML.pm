package ROR2N::View::HTML;

use strict;
use warnings;

use base 'Catalyst::View::TT';

__PACKAGE__->config(
    render_die          => 1,
    ENCODING            => 'utf-8',
    TEMPLATE_EXTENSION  => '.tt2',
    WRAPPER             => 'wrapper.tt2',
    INCLUDE_PATH        => [
        'root/src',
        'root/lib',
        'root/site',
    ],
#    FILTERS => {
#        i18n  => \&i18n,
#    }
);

#sub i18n {
#    my $id  = shift;
#
#    return localize($id);
#}

=head1 NAME

ROR2N::View::HTML - TT View for ROR2N

=head1 DESCRIPTION

TT View for ROR2N.

=head1 SEE ALSO

L<ROR2N>

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;

