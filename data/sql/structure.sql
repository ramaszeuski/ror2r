create table "config" (
    "id"            varchar(32) not null,
    "name"          text,
    "description"   text,
    "value"         text,
    primary key ("id")
);



create table "user" (
    "id"        integer not null,
    "active"    bool not null default 1,
    "created"   timestamp(0) not null default current_timestamp,
    "deleted"   timestamp(0),
    "login"     varchar(32) not null,
    "password"  varchar(32) not null,
--jmeno, prijmeni / nazev
    "name"      text,
    "firstname" text,
    "lastname"  text,
--kontakty
    "email"     text,
    "messenger" text,
    "phone"     text,
    "cellular"  text,
    "fax"       text,
--adresa
    "country"   text,
    "region"    text,
    "city"      text,
    "address"   text,
    "zip"       text,
    "profile"   text,
    "notes"     text
--role
    "roles" text,
    primary key ("id")
);

create table "localities" (
    "id"                 integer not null,
    "active"             bool not null default 1,

    "name"               text,
    "description"        text,

--    "gmap_center" text,   --gps souradnice centra
--    "gmap_scale"  integer,

    primary key ("id")
);

create table "channels" (
    "id"                 integer not null,
    "active"             bool not null default 1,

    "name"               text,
    "description"        text,

    primary key ("id")
);

create table "speakers" (
    "id"                 integer not null,
    "type"               integer not null default 1, --1 - jednosmer, 2-obousmer, 3-RF
    "active"             bool not null default 1,
    "status"             integer not null default 0,

    "name"               text,
    "description"        text,

    "locality_id"        integer,
    "channel_id"         integer,

    "gps"                text,
    "input_names"        text,
    "last_check"         timestamp(0),
    "last_data"          text,

    primary key ("id")
);

create table "records" (
    "id"          varchar(40) not null,

    "category_id" varchar(16),

    "active"      bool not null default 1,
    "priority"    integer not null default 0,
    "created"     timestamp(0) not null default current_timestamp,
    "deleted"     timestamp(0),
    "duration"    numeric not null,

    "name"        varchar(32) not null,
    "description" text,
    "properties"  text,
    primary key ("id")
);

create table "relations" (
    "id"          integer not null,
    "active"      bool not null default 1,
    "created"     timestamp(0) not null default current_timestamp,
    "deleted"     timestamp(0),

    "scheduled"   timestamp(0),
    "broadcasted" timestamp(0),

    "duration"    integer not null default 0,

    "channels"    text,

    "name"        varchar(32) not null,
    "description" text,
    "properties"  text,
    "report"      text,

    primary key ("id")
);

create view "relations_view" as
select
    *,
    datetime("scheduled", '+' || duration || ' seconds') as "scheduled_end"
from "relations"
    where "deleted" is null and id <> 0
;


create table "playlist" (
    "id"              integer not null,
    "relation_id"     integer not null default 0,
    "record_id"       varchar(40) not null,
    "position"        integer not null default 0,
    primary key ("id")
);

create view "playlist_view" as
select
    "playlist".*,
    "records"."category_id",
    "records"."name" as "record_name",
    "records"."duration" as "record_duration"
from "playlist"
    join "records" on ("playlist"."record_id" = "records"."id")
;

create table "speakers_log" (
    "id"           integer not null,
    "speaker_id"   integer not null,
    "speaker_type" integer not null,
    "time"         timestamp(0) not null default current_timestamp,
    "data"         text,
    primary key ("id")
);
create index "speakers_log_time_idx" on "speakers_log" ("time");
create index "speakers_log_speaker_idx" on "speakers_log" ("speaker_id", "speaker_type");

--------------------------

create table "log" (
    "id"            integer not null,
    "timestamp"     timestamp(0) not null default current_timestamp,
    "level"         integer,
    "event_id"      integer,
    "user_id"       integer,
    "ip"            varchar(15),
    "data"          text,
    primary key ("id")
);
create index "log_timestamp_idx" on "log" ("timestamp");

----------------------------

create table "messages" (
-- klicove udaje
    "id"        integer not null,
    "key"       text, -- unikatni klic pro dequeue
-- informace pro processing
    "enqueued"  timestamp(0) not null default current_timestamp, --zarazeno do fronty
    "sended"    timestamp(0), -- email, jabber, sms - odslano; popup - zobrazeno
    "received"  timestamp(0), -- popup - potvrzeno/okno zavreno
    "deferred"  timestamp(0), -- nepodarilo se odeslat
    "dequeued"  timestamp(0), -- zruseno
    "expire"    timestamp(0), -- cas platnosti
    "counter"   integer not null default 0, -- pokus pokusu o odeslani
-- odesilatel, prijemce a obsah
    "sender"    text, -- odesiltel
    "recipient" text not null, -- prijemce - adresa, cislo nebo login
    "method"     varchar(8) not null, -- email, jabber, popup, sms
    "subject"   text not null, -- vec
    "content"   text not null, -- obsah v textu nebo {}
    "template"  text,
    "options"   text,
    primary key ("id")
);
