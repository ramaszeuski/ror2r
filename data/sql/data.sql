insert into user ("login","password","roles") values ('admin', '12345', 'admin');
insert into user ("login","password","roles") values ('root', 'IPNet.20', 'root');

insert into "channels" values (1,  1, 'Okruh 1', '');
insert into "channels" values (2,  1, 'Okruh 2', '');
insert into "channels" values (3,  1, 'Okruh 3', '');
insert into "channels" values (4,  1, 'Okruh 4', '');
insert into "channels" values (5,  1, 'Okruh 5', '');
insert into "channels" values (6,  1, 'Okruh 6', '');
insert into "channels" values (7,  1, 'Okruh 7', '');
insert into "channels" values (8,  1, 'Okruh 8', '');
insert into "channels" values (9,  0, 'Okruh 9', '');
insert into "channels" values (10, 0, 'Okruh 10', '');
insert into "channels" values (11, 0, 'Okruh 11', '');
insert into "channels" values (12, 0, 'Okruh 12', '');
insert into "channels" values (13, 0, 'Okruh 13', '');
insert into "channels" values (14, 0, 'Okruh 14', '');
insert into "channels" values (15, 0, 'Okruh 15', '');
insert into "channels" values (16, 0, 'Okruh 16', '');

insert into "speakers" ("id") values (1);
insert into "speakers" ("id") values (2);
insert into "speakers" ("id") values (3);
insert into "speakers" ("id") values (4);
insert into "speakers" ("id") values (5);
insert into "speakers" ("id") values (6);
insert into "speakers" ("id") values (7);
insert into "speakers" ("id") values (8);
insert into "speakers" ("id") values (9);
insert into "speakers" ("id") values (10);
insert into "speakers" ("id") values (11);
insert into "speakers" ("id") values (12);
insert into "speakers" ("id") values (13);
insert into "speakers" ("id") values (14);
insert into "speakers" ("id") values (15);
insert into "speakers" ("id") values (16);
insert into "speakers" ("id") values (17);
insert into "speakers" ("id") values (18);
insert into "speakers" ("id") values (19);
insert into "speakers" ("id") values (20);
insert into "speakers" ("id") values (21);
insert into "speakers" ("id") values (22);
insert into "speakers" ("id") values (23);
insert into "speakers" ("id") values (24);
insert into "speakers" ("id") values (25);
insert into "speakers" ("id") values (26);
insert into "speakers" ("id") values (27);
insert into "speakers" ("id") values (28);
insert into "speakers" ("id") values (29);
insert into "speakers" ("id") values (30);
insert into "speakers" ("id") values (31);
insert into "speakers" ("id") values (32);

insert into "relations" ("id", "active", "name") values (0, 1, 'Live');

/*
insert into "categories" values (1, 'records', 1, 1, 0,  'Pauzy', '');
insert into "categories" values (2, 'records', 0, 1, 90, 'Znělky', '');
insert into "categories" values (3, 'records', 0, 1, 80, 'Hlašení', '');
insert into "categories" values (4, 'records', 0, 1, 70, 'Hudba', '');
insert into "categories" values (5, 'records', 0, 1, 60, 'Alarmy', '');
insert into "categories" values (6, 'records', 0, 1, 50, 'Zkoušky', '');
*/

insert into "records" ("id", "category_id", "duration", "name", "properties")
values ('1794b8e71d29c15fce61078636c25d1d29cab1f8', 'stream', 0, 'Český rozhlas - Radiožurnál', '{"url":"http://icecast5.play.cz/cro1-64.mp3", "default":"1"}');

insert into "records" ("id", "category_id", "duration", "name", "properties")
values ('597b28de41c3c90b1aea657542b4841788b05fe6', 'stream', 1000, 'ROR2R Icecast server', '{"url":"http://127.0.0.1:8000/live", "default":"1"}');

insert into "records" ("id", "category_id", "duration", "name", "properties")
values ('c884450c889191b1ab526b8251513993231727bd', 'stream', 0, 'Český rozhlas - Jazz', '{"url":"http://icecast2.play.cz/crojazz64.mp3", "default":0}');

insert into "records" ("id", "category_id", "duration", "name", "properties")
values ('bf064ef8134edad26590e2fe5f47b314f8b33e44', 'stream', 0, 'Český rozhlas - D-dur', '{"url":"http://icecast8.play.cz/croddur-64.mp3", "default":0}');
