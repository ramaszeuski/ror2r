$(document).ready(function() {

    $( "#progressbar" ).progressbar({});

    $("#play").click(
        function() {
            MQTTClientCmd('ror/mpd/cmd', 'play');
        }
    );

    $("#pause").click(
        function() {
            MQTTClientCmd('ror/mpd/cmd', 'pause');
        }
    );

    $("#stop").click(
        function() {
            MQTTClientCmd('ror/mpd/cmd', 'stop');
        }
    );

});

