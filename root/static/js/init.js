// Globalni promenne

var Loader;
var new_record = '';
var timeout = {};

// timeouty
var TIMEOUT_BROADCAST  = 60 * 1000;
var TIMEOUT_MICROPHONE =  5 * 1000;
var TIMEOUT_STREAM     = 30 * 1000;
var OFF_PAUSE          = 15 * 1000;

// Tlacitka nahravani
function show_record_buttons( state ) {
    $('#record_control input').hide();
    $('#record_control input.show_' + state ).show();
}

// Neulozene nahravky pri nahravani
function purge_canceled_record() {
    if ( new_record ) {
         $.ajax({
             dataType: "json",
             url: '/records/delete/',
             data: {
                id: new_record,
                hard: 1,
             },
         });
    }
}

function ShowError ( text ) {
    new jBox('Modal',
        {
            title: 'Chyba',
            content: text,
            theme: 'ModalBorder',
            closeOnlick: true,
            closeButton: 'box',
        }
    ).open();
}

function ShowNotice ( text, color ) {
    new jBox('Notice',
        {
            content: text,
            color: color,
        }
    );
}

window.onbeforeunload = function() {
  if ( $('#onair span').hasClass('active')) {
    return true;
  }
}

function setNavigation() {
    var path = window.location.pathname;
    path = decodeURIComponent(path);

    $("nav a").each(function () {
        var href = $(this).attr('href');
        if (path === href) {
            $(this).addClass('active');
//            $(this).closest('li').addClass('active');
        }
    });
}


$(document).ready(function() {
    setNavigation();

    $("body").on("contextmenu",function(e){
        return false;
    });

    Loader = new jBox('Modal', {
        content: '<img src="/img/loader.gif"/>'
    });

});

