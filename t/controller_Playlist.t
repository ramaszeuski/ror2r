use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ROR2N';
use ROR2N::Controller::Playlist;

ok( request('/playlist')->is_success, 'Request should succeed' );
done_testing();
