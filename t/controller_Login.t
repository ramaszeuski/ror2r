use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ROR2N';
use ROR2N::Controller::Login;

ok( request('/login')->is_success, 'Request should succeed' );
done_testing();
