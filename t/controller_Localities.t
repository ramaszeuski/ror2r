use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ROR2N';
use ROR2N::Controller::Localities;

ok( request('/localities')->is_success, 'Request should succeed' );
done_testing();
