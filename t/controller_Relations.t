use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ROR2N';
use ROR2N::Controller::Relations;

ok( request('/relations')->is_success, 'Request should succeed' );
done_testing();
