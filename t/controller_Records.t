use strict;
use warnings;
use Test::More;


use Catalyst::Test 'ROR2N';
use ROR2N::Controller::Records;

ok( request('/records')->is_success, 'Request should succeed' );
done_testing();
