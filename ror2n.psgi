use strict;
use warnings;

use ROR2N;

my $app = ROR2N->apply_default_middlewares(ROR2N->psgi_app);
$app;

